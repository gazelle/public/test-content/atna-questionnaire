/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.preferences;

import net.ihe.gazelle.proxy.admin.gui.AbstractApplicationConfigurationManager;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.util.concurrent.ConcurrentNavigableMap;

/**
 * Created by cel on 23/10/15.
 */
@Name("applicationAttributes")
@AutoCreate
@Scope(ScopeType.APPLICATION)
public class ApplicationAttributes extends AbstractApplicationConfigurationManager {

    public static final String APPLICATION_API_KEY = "application_api_key";
    public static final String ATNA_MODE_ENABLED = "atna_mode_enabled";
    public static final String ATNA_QUESTIONNAIRE_DIRECTORY = "atna_questionnaire_directory";
    public static final String AUDIT_MESSAGE_VALIDATION_XSL = "audit_message_validation_xsl";
    public static final String JAVA_CACERTS_TRUSTSTORE_PWD = "java_cacerts_truststore_pwd";
    public static final String MAIN_CAS_KEYWORD = "main_cas_keyword";
    public static final String MAIN_CAS_NAME = "main_cas_name";
    public static final String MAIN_CAS_URL = "main_cas_url";
    public static final String MAIN_TM_URL = "main_tm_application_url";
    public static final String MAIN_TM_MESSAGE_WS = "main_tm_message_ws";
    public static final String MAIN_TM_PKI_ADMINS = "main_tm_pki_admins";
    public static final String SYSLOG_COLLECTOR_ENABLED = "syslog_collector_enabled";
    public static final String SYSLOG_DIRECTORY = "syslog_directory";
    public static final String TLS_FORWARD_CACHE_SECONDS_DURATION = "tls_forward_cache_seconds_duration";

    public static ApplicationAttributes instance() {
        return (ApplicationAttributes) Component.getInstance("applicationAttributes");
    }

    @Override
    public void resetApplicationConfiguration() {
        super.resetApplicationConfiguration();
        MapDbCache.getPreferenceCache().clear();
    }

    public static String getApplicationApiKey() {
        return getStringPreference(APPLICATION_API_KEY);
    }

    public static Boolean isAtnaModeEnabled() {
        return getBooleanPreference(ATNA_MODE_ENABLED);
    }

    public static String getAtnaQuestionnaireDirectory() {
        return getStringPreference(ATNA_QUESTIONNAIRE_DIRECTORY);
    }

    public static String getAuditMessageValidationXsl() {
        return getStringPreference(AUDIT_MESSAGE_VALIDATION_XSL);
    }

    public static String getJavaCacertsTruststorePwd() {
        return getStringPreference(JAVA_CACERTS_TRUSTSTORE_PWD);
    }

    public static String getMainCasKeyword() { return getStringPreference(MAIN_CAS_KEYWORD); }

    public static String getMainCasName() {
        return getStringPreference(MAIN_CAS_NAME);
    }

    public static String getMainCasUrl() {
        return getStringPreference(MAIN_CAS_URL);
    }

    public static String getMainTMUrl() {
        return getStringPreference(MAIN_TM_URL);
    }

    public static String getMainTmMessageWs() {
        return getStringPreference(MAIN_TM_MESSAGE_WS);
    }

    public static String getMainTmPkiAdmins() {
        return getStringPreference(MAIN_TM_PKI_ADMINS);
    }

    public static boolean isSyslogCollectorEnabled() {
        return getBooleanPreference(SYSLOG_COLLECTOR_ENABLED);
    }

    public static String getSyslogDirectory() {
        return getStringPreference(SYSLOG_DIRECTORY);
    }

    public static Integer getTlsForwardCacheDuration() throws NumberFormatException {
        return getIntegerPreference(TLS_FORWARD_CACHE_SECONDS_DURATION);
    }

    protected static Boolean getBooleanPreference(String key) {
        ConcurrentNavigableMap<String, String> cache = MapDbCache.getPreferenceCache();
        if (!cache.containsKey(key)) {
            String booleanAsString = getApplicationProperty(key);
            if (booleanAsString == null) {
                return false;
            } else {
                cache.put(key, booleanAsString);
            }
        }
        return Boolean.parseBoolean(cache.get(key));
    }

    protected static String getStringPreference(String key) {
        ConcurrentNavigableMap<String, String> cache = MapDbCache.getPreferenceCache();
        if (!cache.containsKey(key)) {
            String value = getApplicationProperty(key);
            if (value == null) {
                return null;
            } else {
                cache.put(key, value);
            }
        }
        return cache.get(key);
    }

    protected static int getIntegerPreference(String key) throws NumberFormatException {
        ConcurrentNavigableMap<String, String> cache = MapDbCache.getPreferenceCache();
        if (!cache.containsKey(key)) {
            String integerAsString = getApplicationProperty(key);
            if (integerAsString == null) {
                return 0;
            } else {
                cache.put(key, integerAsString);
            }
        }
        return Integer.parseInt(cache.get(key));
    }

}
