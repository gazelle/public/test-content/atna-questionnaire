/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.dao;

import net.ihe.gazelle.atna.questionnaire.model.*;

import javax.persistence.EntityManager;
import java.util.List;

public class QuestionnaireInstructionDAO {
    private static final String LANG_EN = "en";
    private EntityManager entityManager;

    public QuestionnaireInstructionDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Instruction getInstructionForSectionLanguage(QuestionnaireSection section, String lang, TestingSession testingSession) {
        InstructionInstance questionnaireInstructionInstance;
        InstructionInstanceQuery query = new InstructionInstanceQuery(entityManager);
        query.section().eq(section);
        query.testingSession().eq(testingSession);
        if (lang != null) {
            query.language().eq(lang);
        } else {
            query.language().eq(LANG_EN);
        }

        InstructionInstance result = query.getUniqueResult();
        Instruction instruction = new Instruction();
        if(result != null) {
            instruction.setId(result.getId());
            instruction.setSection(result.getSection());
            instruction.setContent(result.getContent());
            instruction.setLanguage(result.getLanguage());
        }
        if (lang != null && !lang.equals(LANG_EN) && result == null){
            instruction = getInstructionForSectionLanguage(section, LANG_EN, testingSession);
        }
        return instruction;
    }

    public List<InstructionInstance> getAllInstructionTestingSession(TestingSession testingSession) {
       InstructionInstanceQuery query = new InstructionInstanceQuery(entityManager);

        query.testingSession().eq(testingSession);

        return query.getList();
    }

    public InstructionInstance saveQuestionnaireInstruction(InstructionInstance questionnaireInstructionInstance) {
        InstructionInstance savedQuestionnaireInstructionInstance = entityManager.merge(questionnaireInstructionInstance);
        entityManager.flush();
        return savedQuestionnaireInstructionInstance;
    }
}
