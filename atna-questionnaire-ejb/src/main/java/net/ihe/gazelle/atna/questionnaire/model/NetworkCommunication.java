/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

/**
 * NetworkCommunication.java
 *
 * File generated from the ATNAQuestionnaire::NetworkCommunication uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.atna.questionnaire.model;

// End of user code

import net.ihe.gazelle.tf.ws.data.TFConfigurationTypeWrapper;
import org.hibernate.annotations.Type;
import org.w3c.dom.Node;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Description of the class NetworkCommunication.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NetworkCommunication", propOrder = { "comment", "usage", "actor","type" })
@XmlRootElement(name = "NetworkCommunication")
@Entity
@Table(name="atna_network_communication", schema="public")
@SequenceGenerator(name="atna_network_communication_sequence", sequenceName="atna_network_communication_id_seq", allocationSize=1)
public class NetworkCommunication implements java.io.Serializable, SystemRegistrationComparable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id", unique=true, nullable=false)
	@NotNull
	@GeneratedValue(generator="atna_network_communication_sequence", strategy=GenerationType.SEQUENCE)
	private Integer id;

	@XmlElement(name = "Comment")
	@Lob
	@Type(type = "text")
	private String comment;
	
	@XmlElement(name="usage")
	private String usage;
	
	@XmlAttribute(name = "actor", required = true)
	private String actor;

	@XmlAttribute(name = "type", required = true)
	private ConnectionType type;
	
	@XmlAttribute(name="in_out")
	private InOut inOut;

	@XmlTransient
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="atna_questionnaire_id")
	private ATNAQuestionnaireType atnaQuestionnaire;
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	@Transient
	private org.w3c.dom.Node xmlNodePresentation;

    public NetworkCommunication(TFConfigurationTypeWrapper conf, InOut boundType) {
        this.actor = conf.getActor().getName();
        this.type = ConnectionType.fromValueInTM(conf.getConfigurationType());
        this.usage = conf.getUsage();
        this.atnaQuestionnaire = null;
        this.inOut = boundType;
    }

    public NetworkCommunication(){
		super();
	}

	/**
	 * Return comment.
	 * 
	 * @return comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Set a value to attribute comment.
	 * 
	 * @param comment
	 *            .
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Return actor.
	 * 
	 * @return actor
	 */
	public String getActor() {
		return actor;
	}

	/**
	 * Set a value to attribute actor.
	 * 
	 * @param actor
	 *            .
	 */
	public void setActor(String actor) {
		this.actor = actor;
	}


	/**
	 * Return type.
	 * 
	 * @return type
	 */
	public ConnectionType getType() {
		return type;
	}

	/**
	 * Set a value to attribute type.
	 * 
	 * @param type
	 *            .
	 */
	public void setType(ConnectionType type) {
		this.type = type;
	}

	public Node getXmlNodePresentation() {
		if (xmlNodePresentation == null) {
			xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "NetworkCommunication", this);
		}
		return xmlNodePresentation;
	}

	public void setXmlNodePresentation(Node xmlNodePresentation) {
		this.xmlNodePresentation = xmlNodePresentation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ATNAQuestionnaireType getAtnaQuestionnaire() {
		return atnaQuestionnaire;
	}

	void setAtnaQuestionnaire(ATNAQuestionnaireType atnaQuestionnaire) {
		this.atnaQuestionnaire = atnaQuestionnaire;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	

	public InOut getInOut() {
		return inOut;
	}

	public void setInOut(InOut inOut) {
		this.inOut = inOut;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actor == null) ? 0 : actor.hashCode());
		result = prime * result + ((atnaQuestionnaire == null) ? 0 : atnaQuestionnaire.hashCode());
		result = prime * result + ((inOut == null) ? 0 : inOut.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((usage == null) ? 0 : usage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NetworkCommunication other = (NetworkCommunication) obj;
		if (actor == null) {
			if (other.actor != null)
				return false;
		} else if (!actor.equals(other.actor))
			return false;
		if (atnaQuestionnaire == null) {
			if (other.atnaQuestionnaire != null)
				return false;
		} else if (!atnaQuestionnaire.equals(other.atnaQuestionnaire))
			return false;
		if (inOut != other.inOut)
			return false;
		if (type != other.type)
			return false;
		if (usage == null) {
			if (other.usage != null)
				return false;
		} else if (!usage.equals(other.usage))
			return false;
		return true;
	}

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEquivalent(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        NetworkCommunication other = (NetworkCommunication) obj;
        if (actor == null) {
            if (other.actor != null) {
                return false;
            }
        } else if (!actor.equals(other.actor)) {
            return false;
        }
        if (inOut != other.inOut) {
            return false;
        }
        if (type != other.type) {
            return false;
        }
        if (usage == null) {
            if (other.usage != null) {
                return false;
            }
        } else if (!usage.equals(other.usage)) {
            return false;
        }
        return true;
    }

}