/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.preferences;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.util.concurrent.ConcurrentNavigableMap;

/**
 * Created by jle on 26/10/15.
 */
public final class MapDbCache {

    private volatile static ConcurrentNavigableMap<String, String> treeMapUserSCAS;
    private volatile static ConcurrentNavigableMap<String, String> treeMapPreference;
    private volatile static ConcurrentNavigableMap<String, String> treeMapSyslog;
    private volatile static DB db;

    private MapDbCache() {
        db = DBMaker.newMemoryDB().closeOnJvmShutdown().make();
        treeMapUserSCAS = db.getTreeMap("userSCASCache");
        treeMapPreference = db.getTreeMap("preferenceCache");
        treeMapSyslog = db.getTreeMap("syslogCache");
    }

    public static DB getInstance() {
        if (db == null) {
            new MapDbCache();
        }
        return db;
    }

    public static ConcurrentNavigableMap<String, String> getUserSCASCache() {
        if (treeMapUserSCAS == null) {
            new MapDbCache();
        }
        return treeMapUserSCAS;
    }

    public static ConcurrentNavigableMap<String, String> getPreferenceCache() {
        if (treeMapPreference == null) {
            new MapDbCache();
        }
        return treeMapPreference;
    }

    public static ConcurrentNavigableMap<String, String> getSyslogCache() {
        if (treeMapSyslog == null) {
            new MapDbCache();
        }
        return treeMapSyslog;
    }

    public static void close() {
        getInstance().close();
    }
}
