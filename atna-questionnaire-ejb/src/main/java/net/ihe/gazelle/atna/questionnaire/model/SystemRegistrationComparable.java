/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.model;

public interface SystemRegistrationComparable {

    /**
     * Same as equals, it verifies if 2 objects have same values, except that attributes values that does not comes from Gazelle TM System
     * Registration are not checked.
     * This is used for comparing objects while updating the ATNA Questionnaire from TM registration.
     *
     * @param obj Equivalent object to compare with
     * @return true if both objects are equivalents from TM Registration point of view, false otherwise
     */
    boolean isEquivalent(Object obj);

}
