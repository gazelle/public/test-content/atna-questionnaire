/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.atna.questionnaire.dao.ATNAQuestionnaireTypeDAO;
import net.ihe.gazelle.atna.questionnaire.dao.InstructionDAO;
import net.ihe.gazelle.atna.questionnaire.dao.QuestionnaireInstructionDAO;
import net.ihe.gazelle.atna.questionnaire.menu.ATNAQuestionnairePages;
import net.ihe.gazelle.atna.questionnaire.model.Instruction;
import net.ihe.gazelle.atna.questionnaire.model.QuestionnaireSection;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.web.Locale;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

@Name("atnaQuestionnaireViewer")
@Scope(ScopeType.PAGE)
public class ATNAQuestionnaireViewer extends QuestionnaireFilters implements Serializable, UserAttributeCommon {

    private static final long serialVersionUID = -8844398142409557716L;

    private Instruction inboundNetworkCommunicationsInstructions;
    private Instruction outboundNetworkCommunicationsInstructions;
    private Instruction authenticationProcessInstructions;
    private Instruction auditMessagesInstructions;
    private Instruction accessPhiInstructions;
    private Instruction tlsTestsInstructions;
    private Instruction introductionInstructions;

    @In(value="gumUserService")
    private UserService userService;

    @Create
    public void init() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey("id")) {
            try {
                Integer id = Integer.valueOf(urlParams.get("id"));
                setQuestionnaire(ATNAQuestionnaireTypeDAO.getQuestionnaireById(id));
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Questionnaire's identififer shall be a positive integer");
            }
        }
    }

    public String linkToProofreadMode() {
        return PreferenceService.getString("application_url") + ATNAQuestionnairePages.PROOFREAD_QUESTIONNAIRE
                .getMenuLink() + "?id=" + questionnaireId;
    }

    private Instruction getInstruction(QuestionnaireSection section) {
        QuestionnaireInstructionDAO questionnaireInstructionDAO = new QuestionnaireInstructionDAO(EntityManagerService.provideEntityManager());
        return questionnaireInstructionDAO.getInstructionForSectionLanguage(section, Locale.instance().getLanguage(), getQuestionnaire().getTestingSession());
    }

    public Instruction getInboundNetworkCommunicationsInstructions() {
        return getInstruction(QuestionnaireSection.INBOUND_NETWORK_COMMUNICATION);
    }

    public Instruction getOutboundNetworkCommunicationsInstructions() {
        return getInstruction(QuestionnaireSection.OUTBOUND_NETWORK_COMMUNICATION);
    }

    public Instruction getAuthenticationProcessInstructions() {
        return getInstruction(QuestionnaireSection.AUTHENTICATION_PROCESS);
    }

    public Instruction getAuditMessagesInstructions() {
        return getInstruction(QuestionnaireSection.AUDIT_MESSAGES);
    }

    public Instruction getAccessPhiInstructions() {
        return getInstruction(QuestionnaireSection.ACCESS_PHI);
    }

    public Instruction getTlsTestsInstructions() {
        return getInstruction(QuestionnaireSection.TLS_TESTS);
    }

    public Instruction getIntroductionInstructions() {
        return getInstruction(QuestionnaireSection.INTRODUCTION);
    }

    @Override
    public String getUserName(String userIdWithKey) {
        String[] userAttributes = userIdWithKey.split(" ");
        if (userAttributes.length > 1) {
            return userService.getUserDisplayNameWithoutException(userAttributes[0]) + " " + userAttributes[1];
        }
        return userService.getUserDisplayNameWithoutException(userIdWithKey);
    }
}