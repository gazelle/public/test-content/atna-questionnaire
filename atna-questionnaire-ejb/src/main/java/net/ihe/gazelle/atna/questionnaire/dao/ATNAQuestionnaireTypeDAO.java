/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.dao;

import net.ihe.gazelle.atna.questionnaire.model.ATNAQuestionnaireType;
import net.ihe.gazelle.atna.questionnaire.model.ATNAQuestionnaireTypeQuery;
import net.ihe.gazelle.hql.providers.EntityManagerService;

import javax.persistence.EntityManager;

public class ATNAQuestionnaireTypeDAO {

	public static ATNAQuestionnaireType getATNAQuestionnaireForSystemInSession(String systemKeyword, Integer testingSessionId) {
		ATNAQuestionnaireTypeQuery query = new ATNAQuestionnaireTypeQuery();
		query.system().eq(systemKeyword);
		if (testingSessionId != null) {
			query.testingSession().testingSessionId().eq(testingSessionId);
		}
		return query.getUniqueResult();
	}

	public static ATNAQuestionnaireType mergeEntity(ATNAQuestionnaireType questionnaire) {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		ATNAQuestionnaireType savedEntity = entityManager.merge(questionnaire);
		entityManager.flush();
		return savedEntity;
	}

	public static ATNAQuestionnaireType getQuestionnaireById(Integer id) {
		ATNAQuestionnaireTypeQuery query = new ATNAQuestionnaireTypeQuery();
		query.id().eq(id);
		return query.getUniqueResult();
	}

	public static void deleteATNAQuestionnaire(int questionnaireId) {
		ATNAQuestionnaireType questionnaireToDelete = getQuestionnaireById(questionnaireId);
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		entityManager.remove(questionnaireToDelete);
		entityManager.flush();
	}
}
