/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.atna.questionnaire.dao.ATNAQuestionnaireTypeDAO;
import net.ihe.gazelle.atna.questionnaire.dao.AuditedEventDAO;
import net.ihe.gazelle.atna.questionnaire.dao.NetworkCommunicationDAO;
import net.ihe.gazelle.atna.questionnaire.dao.TLSTestDAO;
import net.ihe.gazelle.atna.questionnaire.model.*;
import net.ihe.gazelle.atna.questionnaire.utils.TestResultProviderClient;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractQuestionnaireEditor extends AbstractQuestionnaireRegistration implements Serializable {

    private static final long serialVersionUID = 3587678687881974572L;

    private static Logger log = LoggerFactory.getLogger(AbstractQuestionnaireEditor.class);

    private static final List<SelectItem> ANSWERS;
    private boolean editMode = false;
    private static List<SelectItem> SYSTEM_TYPES;
    private static List<SelectItem> QUESTIONNAIRE_STATUSES;

    static {
        ANSWERS = new ArrayList<SelectItem>();
        for (BooleanAnswer answer : BooleanAnswer.values()) {
            ANSWERS.add(new SelectItem(answer, answer.getLabel()));
        }
        SYSTEM_TYPES = new ArrayList<SelectItem>();
        for (SystemType type : SystemType.values()) {
            SYSTEM_TYPES.add(new SelectItem(type, type.getValue()));
        }
        QUESTIONNAIRE_STATUSES = new ArrayList<SelectItem>();
        for (Status status : Status.getStatusesForVendor()) {
            QUESTIONNAIRE_STATUSES.add(new SelectItem(status, status.getValue()));
        }
    }

    public abstract String initializeQuestionnaire();

    public List<SelectItem> getAnswers() {
        return ANSWERS;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public List<SelectItem> getSystemTypes() {
        return SYSTEM_TYPES;
    }

    public List<SelectItem> getQuestionnaireStatuses() {
        return QUESTIONNAIRE_STATUSES;
    }

    public void onTlsTestUrlChange(TLSTest test) {
        if (test != null) {
            String url = test.getTestUrl();
            if (url != null && !url.isEmpty()) {
                if (TestResultProviderClient.isAValidUrl(url)) {
                    test.setStatus(TestResultProviderClient.getTlsConnectionResult(url));
                } else {
                    test.setStatus(ValidationStatus.ABORTED);
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            url + " is not a valid permanent link to a TLS connection");
                }
            } else {
                test.setStatus(ValidationStatus.NOTPERFORMED);
            }
            TLSTestDAO.saveEntity(test);
            updateQuestionnaireAudit();
            getTlsTests().resetCache();
        }
    }

    public void save() {
        setQuestionnaire(ATNAQuestionnaireTypeDAO.mergeEntity(getQuestionnaire()));
        setEditMode(false);
    }

    @SuppressWarnings("unchecked")
    public void saveInbounds() {
        List<NetworkCommunication> inboundsList = (List<NetworkCommunication>) getInbounds().getAllItems(getContext());
        NetworkCommunicationDAO.saveEntities(inboundsList);

        updateQuestionnaireAudit();
        getInbounds().resetCache();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Inbound network communication saved.");
    }

    @SuppressWarnings("unchecked")
    public void saveOutbounds() {
        List<NetworkCommunication> outboundsList = (List<NetworkCommunication>) getOutbounds().getAllItems(getContext());
        NetworkCommunicationDAO.saveEntities(outboundsList);

        updateQuestionnaireAudit();
        getOutbounds().resetCache();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Outbound network communication saved.");
    }

    @SuppressWarnings("unchecked")
    public void saveAuditedEvents() {
        List<AuditedEvent> events = ((List<AuditedEvent>) getAuditedEvents().getAllItems(getContext()));
        AuditedEventDAO.saveEntities(events);

        updateQuestionnaireAudit();
        getAuditedEvents().resetCache();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Audit events saved.");
    }

    public void saveTlsTests() {
        List<TLSTest> tlsTests = (List<TLSTest>) getTlsTests().getAllItems(getContext());
        TLSTestDAO.saveEntities(tlsTests);
        updateQuestionnaireAudit();
        getTlsTests().resetCache();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "TLS tests saved.");
    }

    public void markAllEntriesAsProduced() {
        updatedProducedBySystem(BooleanAnswer.YES);
    }

    public void markAllEntriesAsNotProduced() {
        updatedProducedBySystem(BooleanAnswer.NO);
    }

    public void markAllEntriesAsNotAnswered() {
        updatedProducedBySystem(BooleanAnswer.NC);
    }

    private void updatedProducedBySystem(BooleanAnswer produced) {
        @SuppressWarnings("unchecked")
        List<AuditedEvent> events = (List<AuditedEvent>) getAuditedEvents().getAllItems(getContext());
        for (AuditedEvent event : events) {
            event.setProducedBySystem(produced);
            AuditedEventDAO.saveEntity(event);
        }

        updateQuestionnaireAudit();
        getAuditedEvents().resetCache();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Audit events saved.");
    }

    @SuppressWarnings("unchecked")
    private List<NetworkCommunication> getNetworkCommunications(boolean inbound) {
        if (inbound) {
            return (List<NetworkCommunication>) getInbounds().getAllItems(getContext());
        } else {
            return (List<NetworkCommunication>) getOutbounds().getAllItems(getContext());
        }
    }

    /**
     * When referenced other object of the Questionnaire are directly merge into the persistence context
     * (such NetworkCommunications...), the audit attributes "lastModifier" and "lastModifiedOn" are not updated.
     * Call this method (after those objects have been saved) to force the update of the Audit attributes.
     */
    private void updateQuestionnaireAudit() {
        //The detached questionnaire must be reload from the database to avoid previous commited data erasing.
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        setQuestionnaire(entityManager.find(ATNAQuestionnaireType.class, getQuestionnaire().getId()));
        questionnaire.onUpdate();
        save();
    }

}
