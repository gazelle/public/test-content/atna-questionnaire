/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.utils;

import net.ihe.gazelle.atna.questionnaire.model.ValidationStatus;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status;
import java.util.List;

public class TestResultProviderClient {

    private static final String HD_WARNING = "Warning";
    private static final ClientExecutor CLIENT_EXECUTOR = new ApacheHttpClient4Executor();

    private static Logger log = LoggerFactory.getLogger(TestResultProviderClient.class);

    public static ValidationStatus getTlsConnectionResult(String connectionUrl) {
        if (connectionUrl == null) {
            return null;
        } else {
            String id = extractIdFromUrl(connectionUrl);
            String baseUrl = getBaseUrl(connectionUrl);
            if (id != null && baseUrl != null) {
                StringBuilder uriTemplate = new StringBuilder(baseUrl);
                if (connectionUrl.contains("connection")) {
                    uriTemplate.append("/rest/testResult/connection/{id}");
                } else {
                    // test instance
                    uriTemplate.append("/rest/testResult/instance/{id}");
                }
                ClientRequest request = new ClientRequest(uriTemplate.toString(), CLIENT_EXECUTOR);
                request.pathParameter("id", id);
                try {
                    ClientResponse<String> response = request.get(String.class);
                    if (response.getStatus() != Status.OK.getStatusCode()) {
                        MultivaluedMap<String, String> headers = response.getHeaders();
                        List<String> warnings = headers.get(HD_WARNING);
                        if (warnings != null && !warnings.isEmpty()) {
                            log.error(warnings.get(0));
                        } else {
                            log.error("Service returned code " + response.getStatus());
                        }
                        return null;
                    } else {
                        String value = response.getEntity();
                        if (value != null) {
                            return ValidationStatus.fromValue(value);
                        } else {
                            return null;
                        }
                    }
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    return null;
                }
            } else {
                log.error("This is not a permanent link to a TLS connection");
                return null;
            }
        }
    }

    private static String extractIdFromUrl(String url) {
        int paramBegin = url.lastIndexOf('?');
        String params = url.substring(paramBegin + 1);
        String[] values = params.split("&");
        for (String param : values) {
            if (param.startsWith("id=")) {
                return param.substring(param.indexOf('=') + 1);
            }
        }
        return null;
    }

    private static String getBaseUrl(String url) {
        int contextUrlBegin = url.indexOf("/details");
        if (contextUrlBegin == -1) {
            contextUrlBegin = url.indexOf("/testinstance");
        }
        return url.substring(0, contextUrlBegin);
    }

    public static boolean isAValidUrl(String url) {
        if (url != null) {
            String applicationUrl = PreferenceService.getString("application_url");
            String tlsTestUrl = applicationUrl.concat("/details/connection.seam?id=");
            String tlsTestCaseUrl = applicationUrl.concat("/testinstance/view.seam?id=");
            return url.startsWith(tlsTestUrl) || url.startsWith(tlsTestCaseUrl);
        } else {
            return false;
        }
    }

}
