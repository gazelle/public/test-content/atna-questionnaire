/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.modifiers;

import java.util.Map;

import net.ihe.gazelle.atna.questionnaire.model.ATNAQuestionnaireType;
import net.ihe.gazelle.atna.questionnaire.model.AuditedEvent;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.QueryModifier;

public class AuditedEventQueryModifier implements QueryModifier<AuditedEvent> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private ATNAQuestionnaireType questionnaire;

    public AuditedEventQueryModifier(ATNAQuestionnaireType inQuestionnaire) {
        this.questionnaire = inQuestionnaire;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<AuditedEvent> queryBuilder, Map<String, Object> filterValuesApplied) {
        queryBuilder.addEq("atnaQuestionnaire", questionnaire);
    }

}
