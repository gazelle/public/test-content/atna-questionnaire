/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.atna.questionnaire.dao.ATNAQuestionnaireTypeDAO;
import net.ihe.gazelle.atna.questionnaire.model.Status;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("atnaQuestionnaireProofread")
@Scope(ScopeType.PAGE)
public class ATNAQuestionnaireProofread extends AbstractQuestionnaireRegistration implements Serializable {

	private static final long serialVersionUID = -8844398142409557716L;
	private static List<SelectItem> QUESTIONNAIRE_STATUSES;
	
	static{
		QUESTIONNAIRE_STATUSES = new ArrayList<SelectItem>();
		for (Status status: Status.getStatusesForMonitor()){
			QUESTIONNAIRE_STATUSES.add(new SelectItem(status, status.getValue()));
		}
	}

	@Create
	public void init() {
		Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if (urlParams.containsKey("id")) {
			try {
				Integer id = Integer.valueOf(urlParams.get("id"));
				setQuestionnaire(ATNAQuestionnaireTypeDAO.getQuestionnaireById(id));
			} catch (NumberFormatException e) {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR,
						"Questionnaire's identififer shall be a positive integer");
			}
		}
	}
	
	public List<SelectItem> getQuestionnaireStatuses(){
		return QUESTIONNAIRE_STATUSES;
	}
	
	public void save(){
		getQuestionnaire().setReviewer(Identity.instance().getCredentials().getUsername(),
				UserAttributes.instance().getCasKeyword());
		setQuestionnaire(ATNAQuestionnaireTypeDAO.mergeEntity(getQuestionnaire()));
	}
}
