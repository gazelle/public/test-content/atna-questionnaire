/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.dao;

import javax.persistence.EntityManager;

import net.ihe.gazelle.atna.questionnaire.model.TestingSession;
import net.ihe.gazelle.atna.questionnaire.model.TestingSessionQuery;

import java.util.List;

public class TestingSessionDAO {
	private EntityManager entityManager;

	public TestingSessionDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public TestingSession getTestingSessionWithId(Integer id) {
		//EntityManager entityManager = EntityManagerService.provideEntityManager();
		TestingSessionQuery query = new TestingSessionQuery(entityManager);
		query.testingSessionId().eq(id);
		return query.getUniqueResult();
	}

	public List<TestingSession> getAllTestingSession() {
		//EntityManager entityManager = EntityManagerService.provideEntityManager();
		TestingSessionQuery query = new TestingSessionQuery(entityManager);
		query.getQueryBuilder().addOrder("id", false, false);
		return query.getList();
	}

	public TestingSession mergeEntity(TestingSession testingSession) {
		//EntityManager entityManager = EntityManagerService.provideEntityManager();
		TestingSession savedEntity = entityManager.merge(testingSession);
		entityManager.flush();
		return savedEntity;
	}

}
