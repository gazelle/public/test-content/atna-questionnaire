/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.dao;

import net.ihe.gazelle.atna.questionnaire.model.ATNAQuestionnaireType;
import net.ihe.gazelle.atna.questionnaire.model.TLSTest;
import net.ihe.gazelle.atna.questionnaire.model.TLSTestQuery;
import net.ihe.gazelle.hql.providers.EntityManagerService;

import javax.persistence.EntityManager;
import java.util.List;

public class TLSTestDAO {

    public static List<TLSTest> getTlsTestsForQuestionnaire(ATNAQuestionnaireType questionnaire) {
        TLSTestQuery query = new TLSTestQuery();
        query.atnaQuestionnaire().eq(questionnaire);
        return query.getList();
    }

    public static TLSTest getTLSTestById(Integer id) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        return entityManager.find(TLSTest.class, id);
    }

    public static TLSTest saveEntity(TLSTest test) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        TLSTest saved = entityManager.merge(test);
        entityManager.flush();
        return saved;
    }

    public static void saveEntities(List<TLSTest> tests) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        for (TLSTest test : tests) {
            entityManager.merge(test);
        }
        entityManager.flush();
    }

}
