/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.preferences;

import net.ihe.gazelle.cas.client.doubleauthentication.CasLogin;
import net.ihe.gazelle.tm.ws.client.SystemInfoWsClient;
import net.ihe.gazelle.tm.ws.exceptions.GazelleWSException;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

@Name("userAttributes")
@Scope(ScopeType.SESSION)
public class UserAttributes implements Serializable {

    private static final long serialVersionUID = -4745788106074422580L;

    private static Logger log = LoggerFactory.getLogger(UserAttributes.class);

    private String username;
    private String companyKeyword;

    public static UserAttributes instance() {
        return (UserAttributes) Component.getInstance("userAttributes");
    }

    public String getUsername() {
        if (username == null && Identity.instance().isLoggedIn()) {
            username = Identity.instance().getCredentials().getUsername();
        }
        return username;
    }

    public String getCompanyKeyword() {
        if (companyKeyword == null && getUsername() != null) {
            companyKeyword = getCompanyKeywordFromTM();
        }
        return companyKeyword;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setCompanyKeyword(String companyKeyword) {
        this.companyKeyword = companyKeyword;
    }

    private String getCompanyKeywordFromTM() {
        CasLogin userSCas = getUserSCas();
        final String tm = getTMUrl(userSCas);
        if (tm != null) {
            SystemInfoWsClient client = new SystemInfoWsClient(ApplicationAttributes.getApplicationApiKey()) {
                @Override
                public String getTestManagementUrl() {
                    return tm;
                }
            };
            try {
                return client.getInstitutionKeywordForUser(username);
            } catch (GazelleWSException e) {
                log.error("User " + username + " is not registered in this instance of TM: " + tm);
                return null;
            }
        } else {
            log.error("Preference '" + userSCas.getPrefix() + "tm_application_url' is empty !");
            return null;
        }
    }

    public String getCasKeyword() {
        return getCasKeyword(getUserSCas());
    }

    public static String getCasKeyword(CasLogin casLogin) {
        return ApplicationAttributes.getMainCasKeyword();
    }

    public String getTMUrl() {
        return getTMUrl(getUserSCas());
    }

    public static String getTMUrl(CasLogin casLogin) {
        return ApplicationAttributes.getMainTMUrl();
    }

    public String getTMMessageWs() {
        return getTMMessageWs(getUserSCas());
    }

    public static String getTMMessageWs(CasLogin casLogin) {
        return ApplicationAttributes.getMainTmMessageWs();
    }

    public String getTMPkiAdmins() {
        return getTMPkiAdmins(getUserSCas());
    }

    public static String getTMPkiAdmins(CasLogin casLogin) {
        return ApplicationAttributes.getMainTmPkiAdmins();
    }

    public CasLogin getUserSCas() {
        ServletRequest request = (ServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        return getUserCasLoginFromSessionId(getSessionId(request));
    }

    private static CasLogin getUserCasLoginFromSessionId(String sessionId) {
        if (sessionId != null && MapDbCache.getUserSCASCache().containsKey(sessionId)) {
            return CasLogin.getEnum(MapDbCache.getUserSCASCache().get(sessionId));
        } else {
            return CasLogin.MAIN;
        }
    }

    private static String getSessionId(ServletRequest servletRequest) {
        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
            return httpRequest.getSession().getId();
        } else {
            return null;
        }
    }

}
