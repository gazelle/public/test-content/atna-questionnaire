/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.atna.questionnaire.dao.QuestionnaireInstructionDAO;
import net.ihe.gazelle.atna.questionnaire.model.AuditedEvent;
import net.ihe.gazelle.atna.questionnaire.model.Instruction;
import net.ihe.gazelle.atna.questionnaire.model.NetworkCommunication;
import net.ihe.gazelle.atna.questionnaire.model.QuestionnaireSection;
import net.ihe.gazelle.atna.questionnaire.model.SystemRegistrationComparable;
import net.ihe.gazelle.atna.questionnaire.model.TLSTest;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.tm.ws.exceptions.GazelleWSException;
import org.jboss.seam.annotations.In;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.web.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class AbstractQuestionnaireRegistration extends QuestionnaireFilters implements UserAttributeCommon {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractQuestionnaireRegistration.class);

    @In(create = true)
    protected SystemRegistrationProviderLocal systemRegistrationProvider;

    @In(value="gumUserService")
    private UserService userService;

    // display instructions
    private Instruction inboundNetworkCommunicationsInstructions;
    private Instruction outboundNetworkCommunicationsInstructions;
    private Instruction authenticationProcessInstructions;
    private Instruction auditMessagesInstructions;
    private Instruction accessPhiInstructions;
    private Instruction tlsTestsInstructions;
    private Instruction introductionInstructions;

    // temporary lists used for updating mechanism
    private List<NetworkCommunication> inboundNetworkCommunicationsToDelete = new ArrayList<>();
    private List<NetworkCommunication> inboundNetworkCommunicationsToAdd = new ArrayList<>();
    private List<NetworkCommunication> outboundNetworkCommunicationsToDelete = new ArrayList<>();
    private List<NetworkCommunication> outboundNetworkCommunicationsToAdd = new ArrayList<>();
    private List<AuditedEvent> auditedEventsToDelete = new ArrayList<>();
    private List<AuditedEvent> auditedEventsToAdd = new ArrayList<>();
    private List<TLSTest> tlsTestsToDelete = new ArrayList<>();
    private List<TLSTest> tlsTestsToAdd = new ArrayList<>();

    public List<NetworkCommunication> getInboundNetworkCommunicationsToDelete() {
        return inboundNetworkCommunicationsToDelete;
    }

    public List<NetworkCommunication> getInboundNetworkCommunicationsToAdd() {
        return inboundNetworkCommunicationsToAdd;
    }

    public List<NetworkCommunication> getOutboundNetworkCommunicationsToDelete() {
        return outboundNetworkCommunicationsToDelete;
    }

    public List<NetworkCommunication> getOutboundNetworkCommunicationsToAdd() {
        return outboundNetworkCommunicationsToAdd;
    }

    public List<AuditedEvent> getAuditedEventsToDelete() {
        return auditedEventsToDelete;
    }

    public List<AuditedEvent> getAuditedEventsToAdd() {
        return auditedEventsToAdd;
    }

    public List<TLSTest> getTlsTestsToDelete() {
        return tlsTestsToDelete;
    }

    public List<TLSTest> getTlsTestsToAdd() {
        return tlsTestsToAdd;
    }

    public void estimateRegistrationUpdate() {

        List<NetworkCommunication> updatedInboundNetworkCommunications;
        List<NetworkCommunication> updatedOutboundNetworkCommunications;
        List<AuditedEvent> updatedAuditedEvents;

        try {
            updatedInboundNetworkCommunications = systemRegistrationProvider.getInboundNetworkCommunicationForSystem(
                    getQuestionnaire().getSystem(),
                    getQuestionnaire().getTestingSession().getTestingSessionId());
            updatedOutboundNetworkCommunications = systemRegistrationProvider.getOutboundNetworkCommunicationForSystem(
                    getQuestionnaire().getSystem(),
                    getQuestionnaire().getTestingSession().getTestingSessionId());
            updatedAuditedEvents = systemRegistrationProvider.getAuditedEventForSystem(
                    getQuestionnaire().getSystem(),
                    getQuestionnaire().getTestingSession().getTestingSessionId());
        } catch (GazelleWSException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to retrieve system information from TM : " + e
                    .getMessage() + "\nPlease contact an administrator.");
            LOG.error(e.getMessage(), e);
            return;
        }

        computeToAddList(updatedInboundNetworkCommunications, getQuestionnaire().getInboundNetworkCommunications(),
                inboundNetworkCommunicationsToAdd);
        computeToDeleteList(updatedInboundNetworkCommunications, getQuestionnaire().getInboundNetworkCommunications(),
                inboundNetworkCommunicationsToDelete);

        computeToAddList(updatedOutboundNetworkCommunications, getQuestionnaire().getOutboundNetworkCommunications(),
                outboundNetworkCommunicationsToAdd);
        computeToDeleteList(updatedOutboundNetworkCommunications, getQuestionnaire().getOutboundNetworkCommunications(),
                outboundNetworkCommunicationsToDelete);

        computeToAddList(updatedAuditedEvents, getQuestionnaire().getAuditedEvent(), auditedEventsToAdd);
        computeToDeleteList(updatedAuditedEvents, getQuestionnaire().getAuditedEvent(), auditedEventsToDelete);

    }

    public void resetRegistrationUpdate() {
        inboundNetworkCommunicationsToDelete = new ArrayList<>();
        inboundNetworkCommunicationsToAdd = new ArrayList<>();
        outboundNetworkCommunicationsToDelete = new ArrayList<>();
        outboundNetworkCommunicationsToAdd = new ArrayList<>();
        auditedEventsToDelete = new ArrayList<>();
        auditedEventsToAdd = new ArrayList<>();
    }

    public boolean isElementToUpdate() {
        return isElementToAdd() || isElementToDelete();
    }

    public boolean isElementToAdd() {
        return !inboundNetworkCommunicationsToAdd.isEmpty() || !outboundNetworkCommunicationsToAdd.isEmpty() || !auditedEventsToAdd.isEmpty();
    }

    public boolean isElementToDelete() {
        return !inboundNetworkCommunicationsToDelete.isEmpty() || !outboundNetworkCommunicationsToDelete.isEmpty() || !auditedEventsToDelete
                .isEmpty();
    }

    public void estimateTlsTestUpdate() {
        List<TLSTest> updatedTLSTests = TLSTest.computeFromNetworkCommunications(getQuestionnaire().getNetworkCommunications());

        computeToAddList(updatedTLSTests, getQuestionnaire().getTLSTest(), tlsTestsToAdd);
        computeToDeleteList(updatedTLSTests, getQuestionnaire().getTLSTest(), tlsTestsToDelete);
    }

    public void resetTlsTestUpdate() {
        tlsTestsToAdd = new ArrayList<>();
        tlsTestsToDelete = new ArrayList<>();
    }

    public boolean isTlsTestToUpdate() {
        return isTlsTestToAdd() || isTlsTestToDelete();
    }

    public boolean isTlsTestToAdd() {
        return !tlsTestsToAdd.isEmpty();
    }

    public boolean isTlsTestToDelete() {
        return !tlsTestsToDelete.isEmpty();
    }

    @Override
    public String getUserName(String userIdWithKey) {
        String[] userAttributes = userIdWithKey.split(" ");
        if (userAttributes.length > 1) {
            return userService.getUserDisplayNameWithoutException(userAttributes[0]) + " " + userAttributes[1];
        }
        return userService.getUserDisplayNameWithoutException(userIdWithKey);
    }

    private <T extends SystemRegistrationComparable> void computeToAddList(List<T> newList, List<T> existingList, List<T> toAddList) {
        for (T newItem : newList) {
            boolean newItemIsAlreadyPresent = false;
            for (T existingItem : existingList) {
                if (newItem.isEquivalent(existingItem)) {
                    newItemIsAlreadyPresent = true;
                    break;
                }
            }
            if (!newItemIsAlreadyPresent) {
                toAddList.add(newItem);
            }
        }
    }

    private <T extends SystemRegistrationComparable> void computeToDeleteList(List<T> newList, List<T> existingList, List<T> toDeleteList) {
        for (T existingItem : existingList) {
            boolean existingItemIsStillPresent = false;
            for (T newItem : newList) {
                if (existingItem.isEquivalent(newItem)) {
                    existingItemIsStillPresent = true;
                    break;
                }
            }
            if (!existingItemIsStillPresent) {
                toDeleteList.add(existingItem);
            }
        }
    }

    private Instruction getInstruction(QuestionnaireSection section) {
        QuestionnaireInstructionDAO questionnaireInstructionDAO = new QuestionnaireInstructionDAO(EntityManagerService.provideEntityManager());
        return questionnaireInstructionDAO.getInstructionForSectionLanguage(section, Locale.instance().getLanguage(), getQuestionnaire().getTestingSession());
    }

    public Instruction getInboundNetworkCommunicationsInstructions() {
        return getInstruction(QuestionnaireSection.INBOUND_NETWORK_COMMUNICATION);
    }

    public Instruction getOutboundNetworkCommunicationsInstructions() {
        return getInstruction(QuestionnaireSection.OUTBOUND_NETWORK_COMMUNICATION);
    }

    public Instruction getAuthenticationProcessInstructions() {
        return getInstruction(QuestionnaireSection.AUTHENTICATION_PROCESS);
    }

    public Instruction getAuditMessagesInstructions() {
        return getInstruction(QuestionnaireSection.AUDIT_MESSAGES);
    }

    public Instruction getAccessPhiInstructions() {
        return getInstruction(QuestionnaireSection.ACCESS_PHI);
    }

    public Instruction getTlsTestsInstructions() {
        return getInstruction(QuestionnaireSection.TLS_TESTS);
    }

    public Instruction getIntroductionInstructions() {
        return getInstruction(QuestionnaireSection.INTRODUCTION);
    }
}
