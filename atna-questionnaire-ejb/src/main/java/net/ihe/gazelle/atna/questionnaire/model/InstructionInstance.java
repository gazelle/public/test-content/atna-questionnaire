/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

@Entity
@Table(name="atna_instruction_instance", schema="public")
@SequenceGenerator(name="atna_instruction_instance_sequence", sequenceName="atna_instruction_instance_id_seq", allocationSize=1)
@XmlRootElement(name="InstructionInstance")
public class InstructionInstance implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -1L;

    @Id
    @XmlTransient
    @Column(name="id", unique=true, nullable=false)
    @GeneratedValue(generator="atna_instruction_instance_sequence", strategy=GenerationType.SEQUENCE)
    @NotNull
    private Integer id;

    @XmlElement(name = "TestingSession", required = true)
    @JoinColumn(name = "testing_session_id")
    @ManyToOne(cascade = CascadeType.MERGE, targetEntity = TestingSession.class)
    private TestingSession testingSession;

    @Column(name="content")
    @Lob
    @Type(type = "text")
    private String content;

    @Column(name="section")
    @Enumerated(EnumType.STRING)
    private QuestionnaireSection section;

    @Column(name="language")
    private String language;

    public InstructionInstance() {
    }

    public InstructionInstance(QuestionnaireSection inSection, String lang){
        this.section = inSection;
        this.language = lang;
    }

    public InstructionInstance(QuestionnaireSection inSection, String lang, String content, TestingSession testingSession){
        this.section = inSection;
        this.language = lang;
        this.content = content;
        this.testingSession = testingSession;
    }

    public Integer getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public QuestionnaireSection getSection() {
        return section;
    }

    public String getLanguage() {
        return language;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSection(QuestionnaireSection section) {
        this.section = section;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((language == null) ? 0 : language.hashCode());
        result = prime * result + ((section == null) ? 0 : section.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        InstructionInstance other = (InstructionInstance) obj;
        if (language == null) {
            if (other.language != null)
                return false;
        } else if (!language.equals(other.language))
            return false;
        if (section == null) {
            if (other.section != null)
                return false;
        } else if (!section.equals(other.section))
            return false;
        return true;
    }
}
