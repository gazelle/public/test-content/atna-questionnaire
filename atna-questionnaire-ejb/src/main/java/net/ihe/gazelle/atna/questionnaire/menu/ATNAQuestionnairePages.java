/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public enum ATNAQuestionnairePages implements Page {
	
	QUESTIONNAIRES("/questionnaire/browser.seam", "fa-pencil-square-o", "net.ihe.gazelle.questionnaire.ATNAQuestionnaires", ATNAQuestionnaireAuthorizations.LOGGED),
	EDIT_QUESTIONNAIRE("/questionnaire/editor.seam", "", "net.ihe.gazelle.questionnaire.EditQuestionnaire", ATNAQuestionnaireAuthorizations.LOGGED),
	SHOW_QUESTIONNAIRE("/questionnaire/viewer.seam", "", "net.ihe.gazelle.questionnaire.ShowQuestionnaire", ATNAQuestionnaireAuthorizations.LOGGED),
	PROOFREAD_QUESTIONNAIRE("/questionnaire/proofread.seam", "", "net.ihe.gazelle.questionnaire.ProofreadQuestionnaire", ATNAQuestionnaireAuthorizations.MONITOR),
	QUESTIONNAIRE_TEMPLATE("/questionnaire/questionnaireTemplate.seam", "", "net.ihe.gazelle.questionnaire.EditATNAQuestionnaireTemplate", ATNAQuestionnaireAuthorizations.ADMIN);
	
	private String link;

	private Authorization[] authorizations;

	private String label;

	private String icon;
	
	private ATNAQuestionnairePages(String link, String icon, String label, ATNAQuestionnaireAuthorizations... authorizations) {
		this.link = link;
		this.authorizations = authorizations;
		this.label = label;
		this.icon = icon;
	}

	@Override
	public String getId() {
		return name();
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getLink() {
		return link.replace(".seam", ".xhtml");
	}

	@Override
	public String getMenuLink() {
		return link;
	}

	@Override
	public String getIcon() {
		return icon;
	}

	@Override
	public Authorization[] getAuthorizations() {
		return authorizations;
	}

}
