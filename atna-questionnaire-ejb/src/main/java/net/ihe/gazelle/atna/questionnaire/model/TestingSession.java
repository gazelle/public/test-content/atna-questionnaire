/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.model;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.tm.ws.data.TestingSessionWrapper;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="atna_testing_session", schema="public")
@SequenceGenerator(name="atna_testing_session_sequence", sequenceName="atna_testing_session_id_seq", allocationSize=1)
@XmlRootElement(name="TestingSession")
public class TestingSession implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -24980906589302203L;

	@Id
	@XmlTransient
	@Column(name="id", unique=true, nullable=false)
	@GeneratedValue(generator="atna_testing_session_sequence", strategy=GenerationType.SEQUENCE)
	@NotNull
	private Integer id;
	
	@Column(name="testing_session_id")
	@XmlElement(name="id")
	private Integer testingSessionId;
	
	@Column(name="description")
	private String description;

	@XmlTransient
	@OneToMany(mappedBy = "testingSession",fetch = FetchType.LAZY)
	private List<ATNAQuestionnaireType> questionnaires;

	public TestingSession() {
		super();
	}	
	
	public TestingSession(TestingSessionWrapper wrapper){
		if (wrapper != null){
			this.testingSessionId = wrapper.getId();
			this.description = wrapper.getDescription();
		}
	}
	
	public Integer getId() {
		return id;
	}

	public Integer getTestingSessionId() {
		return testingSessionId;
	}

	@FilterLabel
	public String getDescription() {
		return description;
	}

    public List<ATNAQuestionnaireType> getQuestionnaires() {
        return questionnaires;
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TestingSession that = (TestingSession) o;

		if (!id.equals(that.id)) return false;
		if (!testingSessionId.equals(that.testingSessionId)) return false;
		if (!description.equals(that.description)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + testingSessionId.hashCode();
		result = 31 * result + description.hashCode();
		return result;
	}
}
