/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.menu;

import net.ihe.gazelle.atna.questionnaire.preferences.ApplicationAttributes;
import net.ihe.gazelle.common.pages.Authorization;
import org.jboss.seam.security.Identity;

public enum ATNAQuestionnaireAuthorizations implements Authorization {
	
	LOGGED, MONITOR, ADMIN;

	@Override
	public boolean isGranted(Object... context) {
		Boolean isAtna = ApplicationAttributes.isAtnaModeEnabled();
		switch (this) {
		case LOGGED:
			return (isAtna != null && isAtna && Identity.instance().isLoggedIn());
		case MONITOR:
			return (isAtna != null && isAtna && Identity.instance().hasRole("monitor_role"));
		case ADMIN:
			return (isAtna != null && isAtna && Identity.instance().hasRole("admin_role"));
		default:
			return false;
		}
	}

}
