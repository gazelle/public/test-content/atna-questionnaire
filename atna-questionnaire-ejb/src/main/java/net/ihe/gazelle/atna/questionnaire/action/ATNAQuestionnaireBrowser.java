/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.atna.questionnaire.dao.ATNAQuestionnaireTypeDAO;
import net.ihe.gazelle.atna.questionnaire.dao.InstitutionValueProvider;
import net.ihe.gazelle.atna.questionnaire.model.ATNAQuestionnaireType;
import net.ihe.gazelle.atna.questionnaire.model.ATNAQuestionnaireTypeQuery;
import net.ihe.gazelle.atna.questionnaire.modifiers.StatusModifier;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.filter.criterion.ValueFormatter;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.filter.UserValueFormatter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.io.Serializable;

@Name("atnaQuestionnaireBrowser")
@Scope(ScopeType.PAGE)
public class ATNAQuestionnaireBrowser implements Serializable, UserAttributeCommon {

    private static final long serialVersionUID = 6469996291521049343L;
    public static final String CREATOR = "creator";
    private static Logger log = LoggerFactory.getLogger(ATNAQuestionnaireBrowser.class);

    private Filter<ATNAQuestionnaireType> filter;
    private FilterDataModel<ATNAQuestionnaireType> atnaQuestionnaires;
    private int selectedQuestionnnaireId = 0;

    @In(value="gumUserService")
    private UserService userService;

    public Filter<ATNAQuestionnaireType> getFilter() {
        if (filter == null) {
            filter = new Filter<>(getHQLCriterionsForFilter(),
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
            filter.getFormatters().put(CREATOR, new UserValueFormatter(this.filter, CREATOR));
        }
        return filter;
    }

    public FilterDataModel<ATNAQuestionnaireType> getATNAQuestionnaires() {
        if (atnaQuestionnaires == null) {
            atnaQuestionnaires = new FilterDataModel<ATNAQuestionnaireType>(getFilter()) {
                @Override
                protected Object getId(ATNAQuestionnaireType t) {
                    return t.getId();
                }
            };
        }
        return atnaQuestionnaires;
    }

    public int getSelectedQuestionnnaireId() {
        return selectedQuestionnnaireId;
    }

    public void setSelectedQuestionnnaireId(int selectedQuestionnnaireId) {
        this.selectedQuestionnnaireId = selectedQuestionnnaireId;
    }

    public void reset() {
        getFilter().clear();
        getATNAQuestionnaires().resetCache();
    }

    public String showQuestionnaire(ATNAQuestionnaireType questionnaire) {
        return "/questionnaire/viewer.seam?id=" + questionnaire.getId();
    }

    public String editQuestionnaire(ATNAQuestionnaireType questionnaire) {
        return "/questionnaire/editor.seam?id=" + questionnaire.getId();
    }

    public String proofreadQuestionnaire(ATNAQuestionnaireType questionnaire) {
        return "/questionnaire/proofread.seam?id=" + questionnaire.getId();
    }

    public String getSelectedQuestionnaireSystem() {
        if (selectedQuestionnnaireId != 0) {
            ATNAQuestionnaireType questionnaire =
                    EntityManagerService.provideEntityManager().find(ATNAQuestionnaireType.class,
                            selectedQuestionnnaireId);
            return questionnaire.getSystem();
        } else {
            return null;
        }
    }

    @Restrict("#{atnaQuestionnaireAccessControl.isUserAllowedToDelete(atnaQuestionnaireBrowser.getSelectedQuestionnnaireId())}")
    public void deleteQuestionnaire(int questionnaireId) {
        try {
            ATNAQuestionnaireTypeDAO.deleteATNAQuestionnaire(questionnaireId);
            FacesMessages.instance().add(StatusMessage.Severity.INFO, "ATNA Questionnnaire deleted.");
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Unable to delete ATNA Questionnaire (" + e.getMessage() + ").");
            log.error("Error while deleting ATNA Questionnaire.", e);
        }
    }

    private HQLCriterionsForFilter<ATNAQuestionnaireType> getHQLCriterionsForFilter() {
        ATNAQuestionnaireTypeQuery query = new ATNAQuestionnaireTypeQuery();
        HQLCriterionsForFilter<ATNAQuestionnaireType> criteria = query.getHQLCriterionsForFilter();
        criteria.addQueryModifier(StatusModifier.SINGLETON);
        criteria.addPath("system", query.system());
        criteria.addPath("institution", query.company(), null, InstitutionValueProvider.SINGLETON());
        criteria.addPath("status", query.status());
        criteria.addPath("testingSession", query.testingSession());
        criteria.addPath("reviewer", query.reviewer());
        criteria.addPath(CREATOR, query.username());
        criteria.addPath("timestamp", query.lastModifiedDate());
        return criteria;
    }

    @Override
    public String getUserName(String userIdWithKey) {
        String[] userAttributes = userIdWithKey.split(" ");
        if (userAttributes.length > 1) {
            return userService.getUserDisplayNameWithoutException(userAttributes[0]) + " " + userAttributes[1];
        }
        return userService.getUserDisplayNameWithoutException(userIdWithKey);
    }
}
