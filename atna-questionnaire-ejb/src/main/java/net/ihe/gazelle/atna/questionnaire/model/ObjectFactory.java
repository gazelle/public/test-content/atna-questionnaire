/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

@XmlRegistry
public class ObjectFactory {

    private static Logger log = LoggerFactory.getLogger(ObjectFactory.class);

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     */
    public ObjectFactory() {
    }

    public static <T> Node getXmlNodePresentation(String namespace, String localName, T object) {
        Node xmlNodePresentation = null;
        JAXBContext jc;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = null;
        Document doc = null;
        try {
            db = dbf.newDocumentBuilder();
            doc = db.newDocument();
        } catch (ParserConfigurationException e1) {
            log.error(e1.getMessage(), e1);
        }
        try {
            jc = JAXBContext.newInstance("net.ihe.gazelle.atna.questionnaire.model");
            Marshaller m = jc.createMarshaller();
            m.marshal(object, doc);
            if(doc != null) {
                NodeList nodeList = doc.getElementsByTagNameNS(namespace, localName);
                if (nodeList != null && nodeList.getLength() > 0) {
                    xmlNodePresentation = nodeList.item(0);
                }
            }
        } catch (JAXBException e) {
            try {
                db = dbf.newDocumentBuilder();
                xmlNodePresentation = db.newDocument();
            } catch (Exception ee) {
                log.error(ee.getMessage(), e);
            }
        }
        return xmlNodePresentation;
    }

    /**
     * Create an instance of {@link ATNAQuestionnaireType}
     *
     * @return : a new atna questionnaire
     */
    public ATNAQuestionnaireType createATNAQuestionnaireType() {
        return new ATNAQuestionnaireType();
    }

    /**
     * Create an instance of {@link NetworkCommunication}
     *
     * @return : new network communication
     */
    public NetworkCommunication createNetworkCommunication() {
        return new NetworkCommunication();
    }

    /**
     * Create an instance of {@link AuditedEvent}
     *
     * @return : new audit event
     */
    public AuditedEvent createAuditedEvent() {
        return new AuditedEvent();
    }

    /**
     * Create an instance of {@link TLSTest}
     *
     * @return new TLS Test
     */
    public TLSTest createTLSTest() {
        return new TLSTest();
    }

}