/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.atna.questionnaire.model.AuditedEvent;
import net.ihe.gazelle.atna.questionnaire.model.InOut;
import net.ihe.gazelle.atna.questionnaire.model.NetworkCommunication;
import net.ihe.gazelle.atna.questionnaire.preferences.ApplicationAttributes;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.tf.ws.data.AuditMessageWrapper;
import net.ihe.gazelle.tf.ws.data.AuditMessagesWrapper;
import net.ihe.gazelle.tf.ws.data.TFConfigurationTypeWrapper;
import net.ihe.gazelle.tf.ws.data.TFConfigurationTypesWrapper;
import net.ihe.gazelle.tm.ws.client.AuditMessageWsClient;
import net.ihe.gazelle.tm.ws.client.SystemConfigurationWsClient;
import net.ihe.gazelle.tm.ws.client.SystemInfoWsClient;
import net.ihe.gazelle.tm.ws.data.SystemInSessionWrapper;
import net.ihe.gazelle.tm.ws.data.SystemsInSessionWrapper;
import net.ihe.gazelle.tm.ws.exceptions.GazelleWSException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

/**
 * Provide system registration information from Gazelle TM and requried for building an ATNA questionnaire. Information are: systems
 * registered for a testing session for a given user ; inbound/outbound transactions for a given system ; audited events for a given system.
 *
 * @author Cédric EOCHE-DUVAL
 */
@Name("systemRegistrationProvider")
@Scope(ScopeType.PAGE)
@GenerateInterface("SystemRegistrationProviderLocal")
public class SystemRegistrationProvider implements SystemRegistrationProviderLocal {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SystemRegistrationProvider.class);

    private List<SystemInSessionWrapper> systemsForUser;
    private SystemConfigurationWsClient client;

    /**
     * Return the list of registered system in TM for a given user as SelectItem(s)
     *
     * @param userAttributes user atributes
     * @return a list of SelectItem representing the registered systems accessible by the given user
     * @throws GazelleWSException          if an error occurs while contacting Gazelle Test Management
     * @throws SystemRegistrationException if no result match user criteria (i.e if the result list is empty)
     */
    public List<SelectItem> getSystemsInSessionForUserAsItem() throws GazelleWSException, SystemRegistrationException {

        List<SelectItem> systemsForUserItems = new ArrayList<SelectItem>();
        systemsForUserItems.add(new SelectItem(null, "Please select..."));

        if (!getSystemsInSessionForUser().isEmpty()) {
            for (SystemInSessionWrapper systemWrapper : getSystemsInSessionForUser()) {
                systemsForUserItems.add(new SelectItem(systemWrapper, systemWrapper.getKeyword() + " - "
                        + systemWrapper.getTestingSession().getDescription()));
            }
        } else {
            throw new SystemRegistrationException(
                    "No system found, your institution may not have complete its registration for an opened testing session");
        }

        return systemsForUserItems;
    }

    /**
     * Get Inbound Network Communications for a given system registered in Gazelle TM. Informations are retrieve from Gazelle TM.
     *
     * @param systemKeyword    System In Session keyword
     * @param testingSessionId Testing session id
     * @return the list of inbound Network Communications.
     * @throws GazelleWSException if an error occured while contacting Gazelle TM.
     */
    public List<NetworkCommunication> getInboundNetworkCommunicationForSystem(String systemKeyword, Integer testingSessionId)
            throws GazelleWSException {
        List<NetworkCommunication> inbounds = new ArrayList<>();
        for (TFConfigurationTypeWrapper wrapper : getInboundInterfacesForSystem(systemKeyword, testingSessionId)) {
            inbounds.add(new NetworkCommunication(wrapper, InOut.IN));
        }
        return inbounds;
    }

    /**
     * Get Outbound Network Communications for a given system registered in Gazelle TM. Informations are retrieve from Gazelle TM.
     *
     * @param systemKeyword    System In Session keyword
     * @param testingSessionId Testing session id
     * @return the list of outbound Network Communications.
     * @throws GazelleWSException if an error occured while contacting Gazelle TM.
     */
    public List<NetworkCommunication> getOutboundNetworkCommunicationForSystem(String systemKeyword, Integer testingSessionId)
            throws GazelleWSException {
        List<NetworkCommunication> outbounds = new ArrayList<>();
        for (TFConfigurationTypeWrapper wrapper : getOutboundInterfacesForSystem(systemKeyword, testingSessionId)) {
            outbounds.add(new NetworkCommunication(wrapper, InOut.OUT));
        }
        return outbounds;
    }

    /**
     * Get Audited Events for a given system registered in Gazelle TM. Informations are retrieve from Gazelle TM.
     *
     * @param systemKeyword    System In Session keyword
     * @param testingSessionId Testing session id
     * @return the list of Audited Events.
     * @throws GazelleWSException if an error occured while contacting Gazelle TM.
     */
    public List<AuditedEvent> getAuditedEventForSystem(String systemKeyword, Integer testingSessionId) throws GazelleWSException {
        List<AuditedEvent> auditedEvents = new ArrayList<>();
        for (AuditMessageWrapper wrapper : getAuditMessagesForSystem(systemKeyword, testingSessionId)) {
            auditedEvents.add(new AuditedEvent(wrapper));
        }
        return auditedEvents;
    }

    /**
     * Return list of registered system in TM for a given user.
     *
     * @param userAttributes user atributes
     * @return a list of registered systems accessible by the given user, or an empty list if no result match user criteria
     * @throws GazelleWSException if an error occurs while contacting Gazelle Test Management
     */
    public List<SystemInSessionWrapper> getSystemsInSessionForUser() throws GazelleWSException {
        if (systemsForUser == null) {

            String apiKey = ApplicationAttributes.getApplicationApiKey();
            SystemInfoWsClient client = new SystemInfoWsClient(apiKey) {
                @Override
                public String getTestManagementUrl() {
                    return UserAttributes.instance().getTMUrl();
                }
            };
            SystemsInSessionWrapper systemsWrapper = client.getSystemsInSessionForUser(UserAttributes.instance().getUsername());
            if (systemsWrapper == null || systemsWrapper.getSystemsInSession() == null) {
                systemsForUser = new ArrayList<>();
            } else {
                systemsForUser = systemsWrapper.getSystemsInSession();
            }
        }
        return systemsForUser;
    }

    private List<TFConfigurationTypeWrapper> getInboundInterfacesForSystem(String systemKeyword, Integer testingSessionId) throws GazelleWSException {
        SystemConfigurationWsClient client = getSystemConfigurationWsClient();
        TFConfigurationTypesWrapper wrapper = client.listInboundsForSystemInSession(testingSessionId.toString(), systemKeyword, "all");
        return wrapper.getConfigurations();
    }

    private List<TFConfigurationTypeWrapper> getOutboundInterfacesForSystem(String systemKeyword, Integer testingSessionId)
            throws GazelleWSException {
        SystemConfigurationWsClient client = getSystemConfigurationWsClient();
        TFConfigurationTypesWrapper wrapper = client.listOutboundsForSystemInSession(testingSessionId.toString(), systemKeyword, "all");
        return wrapper.getConfigurations();
    }

    private List<AuditMessageWrapper> getAuditMessagesForSystem(String systemKeyword, Integer testingSessionId) throws GazelleWSException {
        AuditMessageWsClient client = new AuditMessageWsClient() {
            @Override
            public String getTestManagementUrl() {
                return UserAttributes.instance().getTMUrl();
            }
        };
        AuditMessagesWrapper wrapper = client.getAuditMessagesForSystem(systemKeyword, testingSessionId);
        return wrapper.getAuditMessages();
    }

    private SystemConfigurationWsClient getSystemConfigurationWsClient() {
        if (client == null) {
            client = new SystemConfigurationWsClient() {
                @Override
                public String getTestManagementUrl() {
                    return UserAttributes.instance().getTMUrl();
                }
            };
        }
        return client;
    }

}
