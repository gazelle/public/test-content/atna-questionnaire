/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.atna.questionnaire.menu.ATNAQuestionnairePages;
import net.ihe.gazelle.atna.questionnaire.model.ATNAQuestionnaireType;
import net.ihe.gazelle.atna.questionnaire.model.AuditedEvent;
import net.ihe.gazelle.atna.questionnaire.model.AuditedEventQuery;
import net.ihe.gazelle.atna.questionnaire.model.InOut;
import net.ihe.gazelle.atna.questionnaire.model.NetworkCommunication;
import net.ihe.gazelle.atna.questionnaire.model.NetworkCommunicationQuery;
import net.ihe.gazelle.atna.questionnaire.model.TLSTest;
import net.ihe.gazelle.atna.questionnaire.model.TLSTestQuery;
import net.ihe.gazelle.atna.questionnaire.modifiers.AuditedEventQueryModifier;
import net.ihe.gazelle.atna.questionnaire.modifiers.NetworkCommunicationQueryModifier;
import net.ihe.gazelle.atna.questionnaire.modifiers.TLSTestQueryModifier;
import net.ihe.gazelle.atna.questionnaire.preferences.ApplicationAttributes;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;

import javax.faces.context.FacesContext;
import java.io.Serializable;

public abstract class QuestionnaireFilters implements Serializable {

    private static final long serialVersionUID = 6226078741106001238L;

    private Filter<NetworkCommunication> inboundFilter;
    private Filter<NetworkCommunication> outboundFilter;
    private Filter<AuditedEvent> eventFilter;
    private Filter<TLSTest> tlsTestFilter;

    private FilterDataModel<NetworkCommunication> inboundDataModel;
    private FilterDataModel<NetworkCommunication> outboundDataModel;
    private FilterDataModel<AuditedEvent> eventDataModel;
    private FilterDataModel<TLSTest> tlsTestDataModel;

    protected int questionnaireId = 0;
    protected ATNAQuestionnaireType questionnaire = null;

    public FacesContext getContext() {
        return FacesContext.getCurrentInstance();
    }

    public Filter<NetworkCommunication> getInboundFilter() {
        if (inboundFilter == null) {
            inboundFilter = new Filter<NetworkCommunication>(getCriteriaForNetworkCommunication(InOut.IN));
        }
        return inboundFilter;
    }

    public Filter<NetworkCommunication> getOutboundFilter() {
        if (outboundFilter == null) {
            outboundFilter = new Filter<NetworkCommunication>(getCriteriaForNetworkCommunication(InOut.OUT));
        }
        return outboundFilter;
    }

    public Filter<AuditedEvent> getEventFilter() {
        if (eventFilter == null) {
            eventFilter = new Filter<AuditedEvent>(getCriteriaForAuditedEvent());
        }
        return eventFilter;
    }

    public Filter<TLSTest> getTlsTestFilter() {
        if (tlsTestFilter == null) {
            tlsTestFilter = new Filter<TLSTest>(getCriteriaForTLSTest());
        }
        return tlsTestFilter;
    }

    public ATNAQuestionnaireType getQuestionnaire() {
        if (questionnaire == null) {
            if (questionnaireId != 0) {
                setQuestionnaire(EntityManagerService.provideEntityManager().find(
                        ATNAQuestionnaireType.class, questionnaireId));
            }
        }
        return questionnaire;
    }

    public void setQuestionnaire(ATNAQuestionnaireType questionnaire) {
        if (questionnaire != null && questionnaire.getId() != null) {
            //int cannot be set to null
            this.questionnaireId = questionnaire.getId();
        } else {
            this.questionnaireId = 0;
        }
        fetchEagerQuestionnaire(questionnaire);
        this.questionnaire = questionnaire;
    }

    public FilterDataModel<NetworkCommunication> getInbounds() {
        if (inboundDataModel == null) {
            inboundDataModel = new FilterDataModel<NetworkCommunication>(getInboundFilter()) {
                @Override
                protected Object getId(NetworkCommunication t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return inboundDataModel;
    }

    public FilterDataModel<NetworkCommunication> getOutbounds() {
        if (outboundDataModel == null) {
            outboundDataModel = new FilterDataModel<NetworkCommunication>(getOutboundFilter()) {
                @Override
                protected Object getId(NetworkCommunication t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return outboundDataModel;
    }

    public FilterDataModel<AuditedEvent> getAuditedEvents() {
        if (eventDataModel == null) {
            eventDataModel = new FilterDataModel<AuditedEvent>(getEventFilter()) {
                @Override
                protected Object getId(AuditedEvent t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return eventDataModel;
    }

    public FilterDataModel<TLSTest> getTlsTests() {
        if (tlsTestDataModel == null) {
            tlsTestDataModel = new FilterDataModel<TLSTest>(getTlsTestFilter()) {
                @Override
                protected Object getId(TLSTest t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return tlsTestDataModel;
    }

    public void clearInboundFilter() {
        getInboundFilter().clear();
        getInbounds().resetCache();
    }

    public void clearOutboundFilter() {
        getOutboundFilter().clear();
        getOutbounds().resetCache();
    }

    public void clearEventFilter() {
        getEventFilter().clear();
        getAuditedEvents().resetCache();
    }

    public void clearTlsTestFilter() {
        getTlsTestFilter().clear();
        getTlsTests().resetCache();
    }

    public void resetAllFilters() {
        clearInboundFilter();
        clearOutboundFilter();
        clearEventFilter();
        clearTlsTestFilter();
    }

    public String getPermanentLink(int questionnaireId) {
        String link = "";
        if (questionnaireId != 0) {
            link = ApplicationAttributes.instance().getApplicationUrl()
                    + ATNAQuestionnairePages.SHOW_QUESTIONNAIRE.getMenuLink() + "?id=" + questionnaireId;
        }
        return link;
    }

    public String linkToList() {
        return PreferenceService.getString("application_url") + ATNAQuestionnairePages.QUESTIONNAIRES.getMenuLink();
    }

    private HQLCriterionsForFilter<NetworkCommunication> getCriteriaForNetworkCommunication(InOut inout) {
        NetworkCommunicationQuery query = new NetworkCommunicationQuery();
        HQLCriterionsForFilter<NetworkCommunication> criterions = query.getHQLCriterionsForFilter();
        criterions.addQueryModifier(new NetworkCommunicationQueryModifier(getQuestionnaire(), inout));
        criterions.addPath("actor" + inout.getLabel(), query.actor());
        criterions.addPath("usage" + inout.getLabel(), query.usage());
        criterions.addPath("type" + inout.getLabel(), query.type());
        return criterions;
    }

    private HQLCriterionsForFilter<AuditedEvent> getCriteriaForAuditedEvent() {
        AuditedEventQuery query = new AuditedEventQuery();
        HQLCriterionsForFilter<AuditedEvent> criterions = query.getHQLCriterionsForFilter();
        criterions.addQueryModifier(new AuditedEventQueryModifier(getQuestionnaire()));
        criterions.addPath("actor", query.issuingActor());
        criterions.addPath("transaction", query.auditedTransaction());
        criterions.addPath("produced", query.producedBySystem());
        criterions.addPath("status", query.validationStatus());
        return criterions;
    }

    private HQLCriterionsForFilter<TLSTest> getCriteriaForTLSTest() {
        TLSTestQuery query = new TLSTestQuery();
        HQLCriterionsForFilter<TLSTest> criterions = query.getHQLCriterionsForFilter();
        criterions.addQueryModifier(new TLSTestQueryModifier(getQuestionnaire()));
        criterions.addPath("side", query.side());
        criterions.addPath("connectionType", query.connectionType());
        criterions.addPath("tlsstatus", query.status());
        return criterions;
    }

    private void fetchEagerQuestionnaire(ATNAQuestionnaireType questionnaire) {
        if (questionnaire != null) {
            if (questionnaire.getAuditedEvent() != null) {
                questionnaire.getAuditedEvent().size();
            }
            if (questionnaire.getInboundNetworkCommunications() != null) {
                questionnaire.getInboundNetworkCommunications().size();
            }
            if (questionnaire.getOutboundNetworkCommunications() != null) {
                questionnaire.getOutboundNetworkCommunications().size();
            }
            if (questionnaire.getTLSTest() != null) {
                questionnaire.getTLSTest().size();
            }
        }
    }
}
