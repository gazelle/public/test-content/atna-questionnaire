/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.atna.questionnaire.dao.ATNAQuestionnaireTypeDAO;
import net.ihe.gazelle.atna.questionnaire.dao.InstructionDAO;
import net.ihe.gazelle.atna.questionnaire.model.Instruction;
import net.ihe.gazelle.atna.questionnaire.model.QuestionnaireSection;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.web.Locale;

@Name("atnaQuestionnaireTemplateEditor")
@Scope(ScopeType.PAGE)
public class ATNAQuestionnaireTemplateEditor extends AbstractQuestionnaireEditor {

	private static final long serialVersionUID = 3587678687852174572L;

	private Instruction inboundNetworkCommunicationsInstructions;
	private Instruction outboundNetworkCommunicationsInstructions;
	private Instruction authenticationProcessInstructions;
	private Instruction auditMessagesInstructions;
	private Instruction accessPhiInstructions;
	private Instruction tlsTestsInstructions;
	private Instruction introductionInstructions;

	@Create
	@Override
	public String initializeQuestionnaire() {
		String lang = Locale.instance().getLanguage();
		inboundNetworkCommunicationsInstructions = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.INBOUND_NETWORK_COMMUNICATION, lang);
		outboundNetworkCommunicationsInstructions = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.OUTBOUND_NETWORK_COMMUNICATION, lang);
		authenticationProcessInstructions = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.AUTHENTICATION_PROCESS, lang);
		auditMessagesInstructions = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.AUDIT_MESSAGES, lang);
		accessPhiInstructions = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.ACCESS_PHI, lang);
		tlsTestsInstructions = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.TLS_TESTS, lang);
		introductionInstructions = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.INTRODUCTION, lang);

		setQuestionnaire(ATNAQuestionnaireTypeDAO.getATNAQuestionnaireForSystemInSession("TEMPLATE", null));
		return null;
	}

	public Instruction getInboundNetworkCommunicationsInstructions() {
		if (inboundNetworkCommunicationsInstructions == null) {
			inboundNetworkCommunicationsInstructions = newInstruction(QuestionnaireSection.INBOUND_NETWORK_COMMUNICATION);
		}
		return inboundNetworkCommunicationsInstructions;
	}

	private Instruction newInstruction(QuestionnaireSection section) {
		Instruction instruction = new Instruction(section, Locale.instance().getLanguage());
		return InstructionDAO.saveInstruction(instruction);
	}

	public Instruction getOutboundNetworkCommunicationsInstructions() {
		if (outboundNetworkCommunicationsInstructions == null) {
			outboundNetworkCommunicationsInstructions = newInstruction(QuestionnaireSection.OUTBOUND_NETWORK_COMMUNICATION);
		}
		return outboundNetworkCommunicationsInstructions;
	}

	public Instruction getAuthenticationProcessInstructions() {
		if (authenticationProcessInstructions == null) {
			authenticationProcessInstructions = newInstruction(QuestionnaireSection.AUTHENTICATION_PROCESS);
		}
		return authenticationProcessInstructions;
	}

	public Instruction getAuditMessagesInstructions() {
		if (auditMessagesInstructions == null) {
			auditMessagesInstructions = newInstruction(QuestionnaireSection.AUDIT_MESSAGES);
		}
		return auditMessagesInstructions;
	}

	public Instruction getAccessPhiInstructions() {
		if (accessPhiInstructions == null) {
			accessPhiInstructions = newInstruction(QuestionnaireSection.ACCESS_PHI);
		}
		return accessPhiInstructions;
	}

	public Instruction getTlsTestsInstructions() {
		if (tlsTestsInstructions == null) {
			tlsTestsInstructions = newInstruction(QuestionnaireSection.TLS_TESTS);
		}
		return tlsTestsInstructions;
	}
}