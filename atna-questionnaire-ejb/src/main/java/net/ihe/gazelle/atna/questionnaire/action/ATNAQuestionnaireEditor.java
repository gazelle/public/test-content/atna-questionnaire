/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.atna.questionnaire.dao.ATNAQuestionnaireTypeDAO;
import net.ihe.gazelle.atna.questionnaire.dao.InstructionDAO;
import net.ihe.gazelle.atna.questionnaire.dao.QuestionnaireInstructionDAO;
import net.ihe.gazelle.atna.questionnaire.dao.TestingSessionDAO;
import net.ihe.gazelle.atna.questionnaire.menu.ATNAQuestionnairePages;
import net.ihe.gazelle.atna.questionnaire.model.*;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.tm.ws.data.SystemInSessionWrapper;
import net.ihe.gazelle.tm.ws.exceptions.GazelleWSException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Name("atnaQuestionnaireEditor")
@Scope(ScopeType.PAGE)
public class ATNAQuestionnaireEditor extends AbstractQuestionnaireEditor {

    private static final Logger LOG = LoggerFactory.getLogger(ATNAQuestionnaireEditor.class);
    private static final long serialVersionUID = 3587678687881974572L;

    private List<SelectItem> systemsInSession;
    private SystemInSessionWrapper selectedSystemInSession;
    private UserAttributes user;

    public List<SelectItem> getSystemsInSession() {
        return systemsInSession;
    }

    public SystemInSessionWrapper getSelectedSystemInSession() {
        return selectedSystemInSession;
    }

    public void setSelectedSystemInSession(SystemInSessionWrapper selectedSystemInSession) {
        this.selectedSystemInSession = selectedSystemInSession;
    }

    @Create
    public void init() {
        user = UserAttributes.instance();
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey("id")) {
            try {
                Integer id = Integer.valueOf(urlParams.get("id"));
                setQuestionnaire(ATNAQuestionnaireTypeDAO.getQuestionnaireById(id));
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Id of a questionnaire shall be a positive integer");
            }
        }
        if (getQuestionnaire() == null) {
            getSystemsFromTM();
        }
    }

    @Restrict("#{identity.loggedIn}")
    private void getSystemsFromTM() {

        try {
            systemsInSession = systemRegistrationProvider.getSystemsInSessionForUserAsItem();
        } catch (GazelleWSException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Unexpected error while trying to reach Gazelle Test Management WS to retrieve systems information. Please contact an " +
                            "administrator: " + e.getMessage());
            LOG.error("Unexpected error while trying to reach Gazelle Test Management WS to retrieve systems information: " + e.getMessage());
        } catch (SystemRegistrationException e) {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, e.getMessage());
        }
    }

    @Override
    public String initializeQuestionnaire() {
        if (selectedSystemInSession != null) {
            setQuestionnaire(ATNAQuestionnaireTypeDAO.getATNAQuestionnaireForSystemInSession(
                    selectedSystemInSession.getKeyword(), selectedSystemInSession.getTestingSession().getId()));
            if (getQuestionnaire() == null) {
                FacesMessages.instance().add(StatusMessage.Severity.INFO,
                        "No ATNA Questionnaire exists for this system, a new one has been created");

                ATNAQuestionnaireType quest = new ATNAQuestionnaireType();
                quest.setUsername(user.getUsername(), user.getCasKeyword());
                quest.setCompany(selectedSystemInSession.getInstitutions().get(0)
                        .getKeyword()); // TODO how do we handle systems owned by several institutions ?
                quest.setSystem(selectedSystemInSession.getKeyword());
                TestingSessionDAO testingSessionDAO = new TestingSessionDAO(EntityManagerService.provideEntityManager());
                TestingSession testingSession = testingSessionDAO.getTestingSessionWithId(selectedSystemInSession
                        .getTestingSession().getId());
                if (testingSession == null) {
                    testingSession = new TestingSession(selectedSystemInSession.getTestingSession());
                    testingSession = testingSessionDAO.mergeEntity(testingSession);

                    generateQuestionnaireInstructionInstance(testingSession);
                }
                quest.setTestingSession(testingSession);
                // merge entity
                setQuestionnaire(quest);
                save();

                // inbounds
                getQuestionnaire().addAllNetworkCommunication(getNetworkCommunicationsFromRegistration(true));

                // outbounds
                getQuestionnaire().addAllNetworkCommunication(getNetworkCommunicationsFromRegistration(false));

                // ATNA messages
                getQuestionnaire().addAllAuditedEvent(getAuditMessagesFromRegistration());

                // TLS Tests
                getQuestionnaire().addAllTLSTest(TLSTest.computeFromNetworkCommunications(getQuestionnaire().getNetworkCommunications()));

                save();
                resetAllFilters();
            }
            return ATNAQuestionnairePages.EDIT_QUESTIONNAIRE.getMenuLink() +
                    "?id=" + getQuestionnaire().getId();
        }
        return null;
    }

    private void generateQuestionnaireInstructionInstance(TestingSession testingSession) {
        InstructionInstance questionnaireInstructionInstance;
        QuestionnaireInstructionDAO questionnaireInstructionDAO = new QuestionnaireInstructionDAO(EntityManagerService.provideEntityManager());

        for (QuestionnaireSection section : QuestionnaireSection.values()) {
            for (Instruction instruction :  InstructionDAO.getInstructionsForSection(section)) {
                questionnaireInstructionInstance = new InstructionInstance(instruction.getSection(), instruction.getLanguage(), instruction.getContent(), testingSession);
                questionnaireInstructionDAO.saveQuestionnaireInstruction(questionnaireInstructionInstance);
            }
        }
    }

    public String confirmRegistrationUpdate() {

        getQuestionnaire().addAllNetworkCommunication(getInboundNetworkCommunicationsToAdd());
        getQuestionnaire().addAllNetworkCommunication(getOutboundNetworkCommunicationsToAdd());
        getQuestionnaire().addAllAuditedEvent(getAuditedEventsToAdd());

        getQuestionnaire().removeAllNetworkCommunication(getInboundNetworkCommunicationsToDelete());
        getQuestionnaire().removeAllNetworkCommunication(getOutboundNetworkCommunicationsToDelete());
        getQuestionnaire().removeAllAuditedEvent(getAuditedEventsToDelete());

        getQuestionnaire().onUpdate();
        save();

        resetAllFilters();
        resetRegistrationUpdate();

        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Atna Questionnaire updated");

        return ATNAQuestionnairePages.EDIT_QUESTIONNAIRE.getMenuLink() +
                "?id=" + getQuestionnaire().getId();
    }

    public void confirmTlsTestUpdate() {
        getQuestionnaire().addAllTLSTest(getTlsTestsToAdd());
        getQuestionnaire().removeAllTLSTest(getTlsTestsToDelete());

        getQuestionnaire().onUpdate();
        save();

        clearTlsTestFilter();
        resetTlsTestUpdate();

        FacesMessages.instance().add(StatusMessage.Severity.INFO, "TLS Tests updated");
    }

    private List<NetworkCommunication> getNetworkCommunicationsFromRegistration(boolean inbound) {
        try {
            if (inbound) {
                return systemRegistrationProvider.getInboundNetworkCommunicationForSystem(
                        selectedSystemInSession.getKeyword(),
                        selectedSystemInSession.getTestingSession().getId());
            } else {
                return systemRegistrationProvider.getOutboundNetworkCommunicationForSystem(
                        selectedSystemInSession.getKeyword(),
                        selectedSystemInSession.getTestingSession().getId());
            }
        } catch (GazelleWSException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to retrieve network communication information from TM : " + e
                    .getMessage() + "\nPlease contact an administrator.");
            LOG.error(e.getMessage(), e);
            return new ArrayList<>();
        }
    }

    private List<AuditedEvent> getAuditMessagesFromRegistration() {
        try {
            return systemRegistrationProvider.getAuditedEventForSystem(
                    selectedSystemInSession.getKeyword(),
                    selectedSystemInSession.getTestingSession().getId());
        } catch (GazelleWSException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to retrieve audited event information from TM : " + e
                    .getMessage() + "\nPlease contact an administrator.");
            LOG.error(e.getMessage(), e);
            return new ArrayList<>();
        }
    }

}
