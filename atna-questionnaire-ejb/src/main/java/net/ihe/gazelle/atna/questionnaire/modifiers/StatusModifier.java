/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.modifiers;

import java.util.Map;

import net.ihe.gazelle.atna.questionnaire.model.ATNAQuestionnaireType;
import net.ihe.gazelle.atna.questionnaire.model.Status;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.QueryModifier;

import org.jboss.seam.security.Identity;

/**
 * we do not want the monitor to access the questionnaires which are not already ready to be reviewed
 * TODO add a restriction on the list of testing session the user is monitor for
 * @author aberge
 *
 */
public class StatusModifier implements QueryModifier<ATNAQuestionnaireType> {

	private static final long serialVersionUID = 1L;
	
	public static final StatusModifier SINGLETON = new StatusModifier();

	@Override
	public void modifyQuery(HQLQueryBuilder<ATNAQuestionnaireType> queryBuilder, Map<String, Object> filterValuesApplied) {
		if (Identity.instance().isLoggedIn() && Identity.instance().hasRole("monitor_role")
				&& !Identity.instance().hasRole("admin_role")) {
			queryBuilder.addIn("status", Status.getStatusesForMonitor());
		}
	}
}
