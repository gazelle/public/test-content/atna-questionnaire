/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.atna.questionnaire.dao.InstructionDAO;
import net.ihe.gazelle.atna.questionnaire.dao.QuestionnaireInstructionDAO;
import net.ihe.gazelle.atna.questionnaire.dao.TestingSessionDAO;
import net.ihe.gazelle.atna.questionnaire.model.Instruction;
import net.ihe.gazelle.atna.questionnaire.model.InstructionInstance;
import net.ihe.gazelle.atna.questionnaire.model.QuestionnaireSection;
import net.ihe.gazelle.atna.questionnaire.model.TestingSession;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.web.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Name("instructionManager")
@Scope(ScopeType.SESSION)
public class InstructionManager implements Serializable {

    private static final String QUESTIONNAIRE_DISPLAY_TLS_TESTS = "questionnaire_display_tls_tests";

    private static final String QUESTIONNAIRE_DISPLAY_OUTBOUNDS = "questionnaire_display_outbounds";

    private static final String QUESTIONNAIRE_DISPLAY_INBOUNDS = "questionnaire_display_inbounds";

    private static final String QUESTIONNAIRE_DISPLAY_AUTHENTICATION_PROCESS = "questionnaire_display_authentication_process";

    private static final String QUESTIONNAIRE_DISPLAY_ACCESS_PHI = "questionnaire_display_access_phi";

    private static final String QUESTIONNAIRE_DISPLAY_AUDIT_MESSAGES = "questionnaire_display_audit_messages";

    /**
     *
     */
    private static final long serialVersionUID = -6952347775868667081L;

    private static Logger log = LoggerFactory.getLogger(InstructionManager.class);

    private Instruction inboundNetworkCommunications;
    private Instruction outboundNetworkCommunications;
    private Instruction authenticationProcess;
    private Instruction auditMessages;
    private Instruction accessPhi;
    private Instruction tlsTests;
    private Instruction introduction;
    private boolean displayInboundNetworkCommunications;
    private boolean displayOutboundNetworkCommunications;
    private boolean displayAuthenticationProcess;
    private boolean displayAccessPhi;
    private boolean displayTlsTests;
    private boolean displayAuditMessages;
    private boolean editMode;

    private TestingSession selectedTestingSessionValue;

    @Create
    public void init() {
        String lang = Locale.instance().getLanguage();
        log.info("selected language: " + lang);

        inboundNetworkCommunications = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.INBOUND_NETWORK_COMMUNICATION, lang);
        outboundNetworkCommunications = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.OUTBOUND_NETWORK_COMMUNICATION, lang);
        authenticationProcess = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.AUTHENTICATION_PROCESS, lang);
        auditMessages = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.AUDIT_MESSAGES, lang);
        accessPhi = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.ACCESS_PHI, lang);
        tlsTests = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.TLS_TESTS, lang);
        introduction = InstructionDAO.getInstructionForSectionLanguage(QuestionnaireSection.INTRODUCTION, lang);
        displayAccessPhi = PreferenceService.getBoolean(QUESTIONNAIRE_DISPLAY_ACCESS_PHI);
        displayAuditMessages = PreferenceService.getBoolean(QUESTIONNAIRE_DISPLAY_AUDIT_MESSAGES);
        displayAuthenticationProcess = PreferenceService.getBoolean(QUESTIONNAIRE_DISPLAY_AUTHENTICATION_PROCESS);
        displayInboundNetworkCommunications = PreferenceService.getBoolean(QUESTIONNAIRE_DISPLAY_INBOUNDS);
        displayOutboundNetworkCommunications = PreferenceService.getBoolean(QUESTIONNAIRE_DISPLAY_OUTBOUNDS);
        displayTlsTests = PreferenceService.getBoolean(QUESTIONNAIRE_DISPLAY_TLS_TESTS);
        editMode = false;
    }

    public void save(Instruction selectedInstruction) {
        InstructionDAO.saveInstruction(selectedInstruction);
        editMode = false;
    }

    public Instruction getInboundNetworkCommunications() {
        if (inboundNetworkCommunications == null) {
            inboundNetworkCommunications = newInstruction(QuestionnaireSection.INBOUND_NETWORK_COMMUNICATION);
        }
        return inboundNetworkCommunications;
    }

    private Instruction newInstruction(QuestionnaireSection section) {
        Instruction instruction = new Instruction(section, Locale.instance().getLanguage());
        return InstructionDAO.saveInstruction(instruction);
    }

    public Instruction getOutboundNetworkCommunications() {
        if (outboundNetworkCommunications == null) {
            outboundNetworkCommunications = newInstruction(QuestionnaireSection.OUTBOUND_NETWORK_COMMUNICATION);
        }
        return outboundNetworkCommunications;
    }

    public Instruction getAuthenticationProcess() {
        if (authenticationProcess == null) {
            authenticationProcess = newInstruction(QuestionnaireSection.AUTHENTICATION_PROCESS);
        }
        return authenticationProcess;
    }

    public Instruction getAuditMessages() {
        if (auditMessages == null) {
            auditMessages = newInstruction(QuestionnaireSection.AUDIT_MESSAGES);
        }
        return auditMessages;
    }

    public Instruction getAccessPhi() {
        if (accessPhi == null) {
            accessPhi = newInstruction(QuestionnaireSection.ACCESS_PHI);
        }
        return accessPhi;
    }

    public Instruction getTlsTests() {
        if (tlsTests == null) {
            tlsTests = newInstruction(QuestionnaireSection.TLS_TESTS);
        }
        return tlsTests;
    }

    public void setInboundNetworkCommunications(Instruction inboundNetworkCommunications) {
        this.inboundNetworkCommunications = inboundNetworkCommunications;
    }

    public void setOutboundNetworkCommunications(Instruction outboundNetworkCommunications) {
        this.outboundNetworkCommunications = outboundNetworkCommunications;
    }

    public void setAuthenticationProcess(Instruction authenticationProcess) {
        this.authenticationProcess = authenticationProcess;
    }

    public void setAuditMessages(Instruction auditMessages) {
        this.auditMessages = auditMessages;
    }

    public void setAccessPhi(Instruction accessPhi) {
        this.accessPhi = accessPhi;
    }

    public void setTlsTests(Instruction tlsTests) {
        this.tlsTests = tlsTests;
    }

    public boolean isDisplayInboundNetworkCommunications() {
        return displayInboundNetworkCommunications;
    }

    public boolean isDisplayOutboundNetworkCommunications() {
        return displayOutboundNetworkCommunications;
    }

    public boolean isDisplayAuthenticationProcess() {
        return displayAuthenticationProcess;
    }

    public boolean isDisplayAccessPhi() {
        return displayAccessPhi;
    }

    public boolean isDisplayTlsTests() {
        return displayTlsTests;
    }

    public boolean isDisplayAuditMessages() {
        return displayAuditMessages;
    }

    public void setDisplayInboundNetworkCommunications(boolean displayInboundNetworkCommunications) {
        this.displayInboundNetworkCommunications = displayInboundNetworkCommunications;
        PreferenceService.setBoolean(QUESTIONNAIRE_DISPLAY_INBOUNDS, displayInboundNetworkCommunications);
    }

    public void setDisplayOutboundNetworkCommunications(boolean displayOutboundNetworkCommunications) {
        this.displayOutboundNetworkCommunications = displayOutboundNetworkCommunications;
        PreferenceService.setBoolean(QUESTIONNAIRE_DISPLAY_OUTBOUNDS, displayOutboundNetworkCommunications);
    }

    public void setDisplayAuthenticationProcess(boolean displayAuthenticationProcess) {
        this.displayAuthenticationProcess = displayAuthenticationProcess;
        PreferenceService.setBoolean(QUESTIONNAIRE_DISPLAY_AUTHENTICATION_PROCESS, displayAuthenticationProcess);
    }

    public void setDisplayAccessPhi(boolean displayAccessPhi) {
        this.displayAccessPhi = displayAccessPhi;
        PreferenceService.setBoolean(QUESTIONNAIRE_DISPLAY_ACCESS_PHI, displayAccessPhi);
    }

    public void setDisplayTlsTests(boolean displayTlsTests) {
        this.displayTlsTests = displayTlsTests;
        PreferenceService.setBoolean(QUESTIONNAIRE_DISPLAY_TLS_TESTS, displayTlsTests);
    }

    public void setDisplayAuditMessages(boolean displayAuditMessages) {
        this.displayAuditMessages = displayAuditMessages;
        PreferenceService.setBoolean(QUESTIONNAIRE_DISPLAY_AUDIT_MESSAGES, displayAuditMessages);
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public Instruction getIntroduction() {
        if (introduction == null) {
            introduction = newInstruction(QuestionnaireSection.INTRODUCTION);
        }
        return introduction;
    }

    public void setIntroduction(Instruction introduction) {
        this.introduction = introduction;
    }

    public void setSelectedTestingSessionValue(TestingSession testingSession) {
        selectedTestingSessionValue = testingSession;
    }

    public TestingSession getSelectedTestingSessionValue() {
        return selectedTestingSessionValue;
    }


    public List<SelectItem> selectedTestingSession() {
        boolean initializeTestintSession = false;
        List<SelectItem> selectedTestingSession = new ArrayList<SelectItem>();

        for (TestingSession testingSession : new TestingSessionDAO(EntityManagerService.provideEntityManager()).getAllTestingSession()) {
            if (!initializeTestintSession) {
                selectedTestingSessionValue = testingSession;
                initializeTestintSession = true;
            }
            selectedTestingSession.add(new SelectItem(testingSession, testingSession.getDescription()));
        }

        return selectedTestingSession;
    }

    public void updateInstructionTestingSession() {
        QuestionnaireInstructionDAO questionnaireInstructionDAO = new QuestionnaireInstructionDAO(EntityManagerService.provideEntityManager());
        for (InstructionInstance questionnaireInstructionInstance : questionnaireInstructionDAO.getAllInstructionTestingSession(selectedTestingSessionValue)) {
            Instruction instruction = InstructionDAO.getInstructionForSectionLanguage(questionnaireInstructionInstance.getSection(), questionnaireInstructionInstance.getLanguage());
            questionnaireInstructionInstance.setContent(instruction.getContent());
            questionnaireInstructionDAO.saveQuestionnaireInstruction(questionnaireInstructionInstance);
        }

    }

}
