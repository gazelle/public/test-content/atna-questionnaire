/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.atna.questionnaire.dao.AuditedEventDAO;
import net.ihe.gazelle.atna.questionnaire.model.AuditedEvent;
import net.ihe.gazelle.atna.questionnaire.model.ValidationStatus;
import net.ihe.gazelle.atna.questionnaire.preferences.ApplicationAttributes;
import net.ihe.gazelle.auditMessage.validator.api.AuditMessageValidationService;
import net.ihe.gazelle.common.fineuploader.FineuploaderListener;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.util.GazellePosixFilePermissions;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

/**
 * AuditMessageIOManager is responsible for ATNA-questionnaire :
 * 1. audit-messages sample import/storage.
 * 2. audit-messages sample validation.
 * 3. audit-message validation result storage.
 * 4. audit-message sample and validation result delivery for view.
 */
@Name("auditMessageIOManager")
@GenerateInterface("AuditMessageIOManagerLocal")
@Scope(ScopeType.SESSION)
public class AuditMessageIOManager implements FineuploaderListener, AuditMessageIOManagerLocal {

    private static Logger log = LoggerFactory.getLogger(AuditMessageIOManager.class);
    private static String baseDir;
    private static final String AUDIT_MESSAGE_DIR = "audit_messages";
    private transient ByteArrayOutputStream outputStream;

    @Destroy
    @Remove
    public void remove() {

    }

    @Create
    public void init() {
        initBaseDir();
    }

    public static synchronized void initBaseDir() {
        baseDir = ApplicationAttributes.getAtnaQuestionnaireDirectory();
        if (baseDir == null) {
            log.error(
                    "No directory set for files to be linked to ATNA questionnaires, set atna_questionnaire_directory");
        } else {
            File baseDir = new File(AuditMessageIOManager.baseDir);
            if (!baseDir.exists()) {
                log.error(
                        "The atna-questionnaire base directory '" + AuditMessageIOManager.baseDir + "' does not exists.");
            } else {
                AuditMessageIOManager.baseDir = AuditMessageIOManager.baseDir + File.separatorChar + AUDIT_MESSAGE_DIR;
                baseDir = new File(AuditMessageIOManager.baseDir);
                if (!baseDir.exists()) {
                    baseDir.mkdir();
                    GazellePosixFilePermissions.setJBossPermissions(baseDir.getPath());
                }
            }
        }
    }

    @Override
    @Transactional
    public void uploadedFile(File tmpFile, String filename, String id, String param) throws IOException {
        synchronized (log) {
            AuditedEvent event = AuditedEventDAO.getAuditedEventById(Integer.parseInt(param));
            if (event != null) {

                StringBuilder storageRelativeFilePath = new StringBuilder(storageRelativeDirPathBuilder());

                storageRelativeFilePath.append(File.separatorChar);
                storageRelativeFilePath.append(event.getId().toString());
                storageRelativeFilePath.append('_');
                storageRelativeFilePath.append(filename);

                event.setFileLocation(storageRelativeFilePath.toString());
                saveFileUploaded(event.getFileLocation(), tmpFile);

                if (event.getValidatorOid() != null) {
                    validateAuditMessage(event);
                } else {
                    event.setValidationStatus(ValidationStatus.NOTAVAILABLE);
                }
                AuditedEventDAO.saveEntity(event);
            } else {
                log.error("No audit message found with id " + param);
            }
        }
    }

    /**
     * build directory arborescence ${questionnaire_storage}/audit_message/${year}/${month}/${day} for storing auditmessages files and validation log.
     *
     * @return String : the storage relative dir location : ${year}/${month}/${day}
     */
    private String storageRelativeDirPathBuilder() {

        Calendar calendar = Calendar.getInstance();
        Integer year = calendar.get(Calendar.YEAR);
        Integer month = calendar.get(Calendar.MONTH) + 1;
        Integer day = calendar.get(Calendar.DAY_OF_MONTH);

        StringBuilder dirLocation = new StringBuilder(year.toString());
        gazelleMkDir(baseDir + File.separatorChar + dirLocation.toString());

        dirLocation.append(File.separatorChar);
        dirLocation.append(month.toString());
        gazelleMkDir(baseDir + File.separatorChar + dirLocation.toString());

        dirLocation.append(File.separatorChar);
        dirLocation.append(day.toString());
        gazelleMkDir(baseDir + File.separatorChar + dirLocation.toString());

        return dirLocation.toString();
    }

    /**
     * If the directory does not exists, it creates it and set POSIX properties permissions (rwxrwxr-x) AND group
     * (jboss-admin)
     *
     * @param path
     * @note This method exists because the recursive method java.nio.Files.createDirectories allows to set permissions
     * but not user group.
     */
    private void gazelleMkDir(String path) {
        File dirFile = new File(path);
        if (!dirFile.exists()) {
            dirFile.mkdir();
            GazellePosixFilePermissions.setJBossPermissions(dirFile.getPath());
        }
    }

    private void validateAuditMessage(AuditedEvent event) {
        String logPath = event.getFileLocation().replace(".xml", "_log.xml");
        event.setValidationLogPath(logPath);
        File logFile = new File(baseDir + File.separatorChar + logPath);

        try (FileOutputStream fos = new FileOutputStream(logFile)) {
            String stylesheetUrl = ApplicationAttributes.getAuditMessageValidationXsl();
            String status = AuditMessageValidationService.validateAuditMessageAndSaveFormattedReport(readFile(event),
                    event.getValidatorOid(), fos, stylesheetUrl);
            if (status != null) {
                GazellePosixFilePermissions.setJBossPermissions(baseDir + File.separatorChar + logPath);
                event.setValidationStatus(ValidationStatus.fromValue(status));
                event.setValidationDate(new Date());
            } else {
                event.setValidationStatus(ValidationStatus.ABORTED);
            }
        } catch (Exception e) {
            event.setValidationStatus(ValidationStatus.ABORTED);
            log.error(e.getMessage());
        }
    }

    private String readFile(AuditedEvent event) {
        String filepath = baseDir + java.io.File.separatorChar + event.getFileLocation();
        File message = new File(filepath);
        if (message.exists()) {
            long length = message.length();
            byte[] fileContent = new byte[(int) length];
            FileInputStream fis = null;
            ByteArrayOutputStream baos = null;
            String content = null;
            try {
                fis = new FileInputStream(message);
                fis.read(fileContent);
                baos = new ByteArrayOutputStream();
                baos.write(fileContent);
                content = baos.toString();
                baos.close();
            } catch (FileNotFoundException e1) {
                FacesMessages.instance().add(StatusMessage.Severity.WARN, e1.getMessage());
                log.error(e1.getMessage());
            } catch (IOException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
                log.error(e.getMessage(), e);
            } finally {
                closeQuietly(fis);
            }
            return content;
        } else {
            String msg = "File at " + filepath + " does not exist";
            FacesMessages.instance().add(StatusMessage.Severity.WARN, msg);
            log.error(msg);
        }
        return null;
    }


    public void saveFileUploaded(String newLocation, File tmpFile) throws IOException {
        InputStream inputStream = new FileInputStream(tmpFile);
        saveFileUploaded(newLocation, inputStream);
        tmpFile.delete();
    }

    public void saveFileUploaded(String newLocation, InputStream inputStream) {
        String filePathForThisFileInstance = baseDir + java.io.File.separatorChar + newLocation;
        java.io.File fileToStore = new java.io.File(filePathForThisFileInstance);
        FileOutputStream fos;
        try {
            if (fileToStore.exists()) {
                fileToStore.delete();
            }
            fos = new FileOutputStream(fileToStore);
            IOUtils.copy(inputStream, fos);
            inputStream.close();
            fos.close();
            GazellePosixFilePermissions.setJBossPermissions(filePathForThisFileInstance);
        } catch (FileNotFoundException e) {
            log.error("", e);
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, e.getMessage());
        } catch (IOException e) {
            log.error("", e);
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, e.getMessage());
        }
    }

    public void viewAuditMessage(AuditedEvent event) {
        if (event != null && event.getFileLocation() != null) {
            viewFile(event.getFileLocation(), event.getFileName());
        } else {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "No file to display");
        }
    }

    public void viewValidationResult(AuditedEvent event) {
        if (event != null && event.getValidationLogPath() != null) {
            viewFile(event.getValidationLogPath(), "log_" + event.getFileName());
        } else {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.WARN, "No file to display");
        }
    }

    private void viewFile(String filepath, String filename) {
        File file = new File(new File(baseDir), filepath);
        if (file.exists()) {
            long length = file.length();
            byte[] fileContent = new byte[(int) length];
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(file);
                fis.read(fileContent);
                getOutputStream().write(fileContent);
                export("application/xml", filename);
            } catch (FileNotFoundException e1) {
                FacesMessages.instance().add(StatusMessage.Severity.WARN, e1.getMessage());
                log.error(e1.getMessage());
            } catch (IOException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
                log.error(e.getMessage(), e);
            } finally {
                closeQuietly(fis);
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "The system has not been able to retrieve the file");
        }
    }

    private void export(String contentType, String fileName) throws IOException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();

        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        response.setContentType(contentType);
        if (fileName != null) {
            response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");
        }
        ServletOutputStream servletOutputStream = response.getOutputStream();
        write(servletOutputStream);
        servletOutputStream.flush();
        servletOutputStream.close();
        facesContext.responseComplete();
    }

    private void write(OutputStream os) throws IOException {
        os.write(getOutputStream().toByteArray(), 0, getOutputStream().size());
        getOutputStream().reset();
        getOutputStream().close();
    }

    public ByteArrayOutputStream getOutputStream() {
        if (outputStream == null) {
            outputStream = new ByteArrayOutputStream();
        }
        return outputStream;
    }

    /**
     * Close a possibly null stream and ignoring IOExceptions.<br/><b>Only use this method on streams where IOExceptions
     * really are irrelevant.</b> As example, do not use this method on an OutputStream there is a "once in a blue moon"
     * chance that an exception will be thrown due to disc errors or file system full. In this case, you could flush the
     * stream before to trigger any possible error.
     *
     * @param closeable
     */
    private void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException ex) {
                //ignore
            }
        }
    }
}
