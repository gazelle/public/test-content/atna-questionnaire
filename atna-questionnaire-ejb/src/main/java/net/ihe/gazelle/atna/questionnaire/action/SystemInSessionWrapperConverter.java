/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.action;

import net.ihe.gazelle.tm.ws.data.SystemInSessionWrapper;
import net.ihe.gazelle.tm.ws.exceptions.GazelleWSException;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

@Name("systemInSessionWrapperConverter")
@BypassInterceptors
@Converter(forClass = SystemInSessionWrapper.class)
public class SystemInSessionWrapperConverter implements javax.faces.convert.Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s != null) {
            SystemRegistrationProviderLocal systemsProvider = (SystemRegistrationProviderLocal) Component
                    .getInstance("systemRegistrationProvider");
            try {
                for (SystemInSessionWrapper system : systemsProvider.getSystemsInSessionForUser()) {
                    if (getAsString(null, null, system).equals(s)) {
                        return system;
                    }
                }
            } catch (GazelleWSException e) {
                //do nothing, return null
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o != null) {
            return "$(" + ((SystemInSessionWrapper) o).getKeyword() + ")@(" + ((SystemInSessionWrapper) o).getTestingSession().getId() + ")";
        } else {
            return null;
        }
    }

}
