/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.dao;

import net.ihe.gazelle.atna.questionnaire.model.ATNAQuestionnaireType;
import net.ihe.gazelle.atna.questionnaire.model.ATNAQuestionnaireTypeQuery;
import net.ihe.gazelle.atna.questionnaire.model.ConnectionType;
import net.ihe.gazelle.atna.questionnaire.model.InOut;
import net.ihe.gazelle.atna.questionnaire.model.NetworkCommunication;
import net.ihe.gazelle.atna.questionnaire.model.NetworkCommunicationQuery;
import net.ihe.gazelle.hql.providers.EntityManagerService;

import javax.persistence.EntityManager;
import java.util.List;

public class NetworkCommunicationDAO {

    public static List<ConnectionType> getInboundTypesProtectedByTLS(ATNAQuestionnaireType questionnaire) {
        ATNAQuestionnaireTypeQuery query = new ATNAQuestionnaireTypeQuery();
        query.id().eq(questionnaire.getId());
        query.networkCommunications().inOut().eq(InOut.IN);
        return query.networkCommunications().type().getListDistinct();
    }

    public static List<ConnectionType> getOutboundTypesProtectedByTLS(ATNAQuestionnaireType questionnaire) {
        ATNAQuestionnaireTypeQuery query = new ATNAQuestionnaireTypeQuery();
        query.id().eq(questionnaire.getId());
        query.networkCommunications().inOut().eq(InOut.OUT);
        return query.networkCommunications().type().getListDistinct();
    }

    public static List<NetworkCommunication> getOutboundsForQuestionnaire(ATNAQuestionnaireType atnaQuestionnaireType) {
        NetworkCommunicationQuery query = new NetworkCommunicationQuery();
        query.atnaQuestionnaire().eq(atnaQuestionnaireType);
        query.inOut().eq(InOut.OUT);
        return query.getList();
    }

    public static List<NetworkCommunication> getInboundsForQuestionnaire(ATNAQuestionnaireType atnaQuestionnaireType) {
        NetworkCommunicationQuery query = new NetworkCommunicationQuery();
        query.atnaQuestionnaire().eq(atnaQuestionnaireType);
        query.inOut().eq(InOut.IN);
        return query.getList();
    }

    public static NetworkCommunication saveEntity(NetworkCommunication nc) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        NetworkCommunication saved = entityManager.merge(nc);
        entityManager.flush();
        return saved;
    }

    public static void saveEntities(List<NetworkCommunication> ncs) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        for (NetworkCommunication nc : ncs) {
            entityManager.merge(nc);
        }
        entityManager.flush();
    }
}
