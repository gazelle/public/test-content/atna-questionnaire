/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.dao;

import javax.persistence.EntityManager;

import net.ihe.gazelle.atna.questionnaire.model.Instruction;
import net.ihe.gazelle.atna.questionnaire.model.InstructionQuery;
import net.ihe.gazelle.atna.questionnaire.model.QuestionnaireSection;
import net.ihe.gazelle.hql.providers.EntityManagerService;

import java.util.List;

public class InstructionDAO {
	
	private static final String LANG_EN = "en";

	public static Instruction getInstructionForSectionLanguage(QuestionnaireSection section, String lang) {
		InstructionQuery query = new InstructionQuery();
		query.section().eq(section);
		if (lang != null) {
			query.language().eq(lang);
		} else {
			query.language().eq(LANG_EN);
		}
		Instruction result = query.getUniqueResult();
		if (lang != null && !lang.equals(LANG_EN) && result == null){
			result = getInstructionForSectionLanguage(section, LANG_EN);
		}
		return result;
	}

	public static List<Instruction> getInstructionsForSection(QuestionnaireSection section) {
		InstructionQuery query = new InstructionQuery();
		query.section().eq(section);

		return query.getList();
	}

	public static List<Instruction> getAllInstruction() {
		InstructionQuery query = new InstructionQuery();
		return query.getList();
	}

	public static Instruction saveInstruction(Instruction instruction) {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		Instruction savedInstruction = entityManager.merge(instruction);
		entityManager.flush();
		return savedInstruction;
	}
}
