/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.menu;

import net.ihe.gazelle.atna.questionnaire.model.ATNAQuestionnaireType;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.validation.Assertion;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Map;

@Name("atnaQuestionnaireAccessControl")
@Scope(ScopeType.EVENT)
public class ATNAQuestionnaireAccessControl {

    private static final String INSITUTION_KEYWORD_CAS_ATTRIBUTE = "institution_keyword";
    @In
    private EntityManager entityManager;
    private String identityUsernameWithKey;
    private Boolean monitor;
    private Boolean admin;

    public boolean isUserAllowedToView(int questionnaireId) {
        return isUserAllowedToEdit(questionnaireId) || isMonitor();
    }

    public boolean isUserAllowedToReview(int questionnaireId) {
        boolean isAllowed = false;
        if (isAdmin() || isMonitor()) {
            isAllowed = true;
        }
        return isAllowed;
    }

    public boolean isUserAllowedToEdit(int questionnaireId) {
        boolean isAllowed = false;
        if (questionnaireId != 0) {
            if (isAdmin()) {
                isAllowed = true;
            } else {
                ATNAQuestionnaireType questionnaire =
                        entityManager.find(ATNAQuestionnaireType.class, questionnaireId);
                if (questionnaire.getCompany() != null && questionnaire.getCompany().equals(getUserInstitutionKeyword())) {
                    isAllowed = true;
                }
            }
        } else {
            isAllowed = true;
        }
        return isAllowed;
    }

    public boolean isUserAllowedToDelete(int questionnaireId) {
        boolean isAllowed = false;
        if (isAdmin()) {
            isAllowed = true;
        } else {
            String currentUser = getIdentityUsernameWithKey();
            if (currentUser != null) {
                ATNAQuestionnaireType questionnaire =
                        entityManager.find(ATNAQuestionnaireType.class, questionnaireId);
                String owner = questionnaire.getUsernameWithKey();
                if (currentUser.equals(owner)) {
                    isAllowed = true;
                }
            }
        }
        return isAllowed;
    }

    private String getIdentityUsernameWithKey() {
        if (identityUsernameWithKey == null) {
            Identity identity = Identity.instance();
            if (identity.getPrincipal() != null) {
                String currentUsername = identity.getPrincipal().getName();
                String currentUserCasKey = UserAttributes.instance().getCasKeyword();
                identityUsernameWithKey = currentUsername + " (" + currentUserCasKey + ")";
            }
        }
        return identityUsernameWithKey;
    }

    private boolean isAdmin() {
        if (admin == null) {
            admin = Identity.instance().hasRole("admin_role");
        }
        return admin;
    }

    private boolean isMonitor() {
        if (monitor == null) {
            monitor = Identity.instance().hasRole("monitor_role");
        }
        return monitor;
    }

    private String getUserInstitutionKeyword() {
        AttributePrincipal attributePrincipal = null;
        Principal casPrincipal = getCASPrincipal();
        if (casPrincipal instanceof AttributePrincipal) {
            attributePrincipal = (AttributePrincipal) casPrincipal;
            Map attributes = attributePrincipal.getAttributes();
            return (String) attributes.get(INSITUTION_KEYWORD_CAS_ATTRIBUTE);
        } else {
            return null;
        }
    }

    private Principal getCASPrincipal() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession(false);
        Assertion assertion = (Assertion) ((Assertion) (session == null ? request.getAttribute("_const_cas_assertion_") : session
                .getAttribute("_const_cas_assertion_")));
        return assertion == null ? null : assertion.getPrincipal();
    }

}
