/*
 *    Copyright 2014-2023 IHE International
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.ihe.gazelle.atna.questionnaire.dao;

import net.ihe.gazelle.atna.questionnaire.model.AuditedEvent;
import net.ihe.gazelle.atna.questionnaire.model.AuditedEventQuery;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.List;

public class AuditedEventDAO {


    private static Logger log = LoggerFactory.getLogger(AuditedEventDAO.class);

    public static AuditedEvent getAuditedEventById(Integer id) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        try {
            return entityManager.find(AuditedEvent.class, id);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public static List<AuditedEvent> getAuditedEventsForQuestionnaire(Integer id) {
        AuditedEventQuery query = new AuditedEventQuery();
        query.atnaQuestionnaire().id().eq(id);
        return query.getList();
    }

    public static AuditedEvent saveEntity(AuditedEvent event) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        AuditedEvent savedEvent = entityManager.merge(event);
        entityManager.flush();
        return savedEvent;
    }

    public static void saveEntities(List<AuditedEvent> auditedEvents) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        for (AuditedEvent event : auditedEvents) {
            entityManager.merge(event);
        }
        entityManager.flush();
    }
}
